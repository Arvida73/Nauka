package Przyklad_String;

public class Zadanie3 {
    public static void main(String[] args) {
        String text = "priorytetowy";

        int liczbaR = 0;
        int liczbaW = 0;

        for (char litera : text.toCharArray()) {
            if (litera == 'r') {
                liczbaR++;
            }
            if (litera == 'w') {
                liczbaW++;
            }
        }

        System.out.println("Liczba liter 'r': " + liczbaR);
        System.out.println("Liczba liter 'w': " + liczbaW);
    }
}
