package Przyklad_String;

public class Zadanie7 {
    public static void main(String[] args) {
        String text = "aabbccbbaa";

        char[] litery = text.toCharArray();

        boolean czyPalindrom = true;
        for (int i = 0; i < litery.length / 2; i++) {
            if (litery[i] != litery[litery.length - 1 - i]) {
                czyPalindrom = false;
                break;
            }
        }

        System.out.println("Text: " + text + ". Palindrom: " + czyPalindrom);
    }
}
