package Przyklad_String;

import java.util.Scanner;

public class Zadanie9 {
    public static void main(String[] args) {
        String text = "aaabbbbaaadddbbaaeee";

        int najdluzszeWystapienie = 0;

        for (int i = 0; i < text.length();) {
            int iloscWystapien = 1;
            char litera = text.charAt(i);
            ++i;
            while (i < text.length() && litera == text.charAt(i)) {
                ++iloscWystapien;
                ++i;
            }
            if (najdluzszeWystapienie < iloscWystapien) {
                najdluzszeWystapienie = iloscWystapien;
            }
        }

        System.out.println("Najdluzsze wystapienie litery: " + najdluzszeWystapienie);
    }
}
