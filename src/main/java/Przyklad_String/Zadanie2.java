package Przyklad_String;

import java.util.Scanner;

public class Zadanie2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj ilosc linii: ");
        int iloscLinii = scanner.nextInt();
        scanner.nextLine();

        String polaczonyText = "";
        for (int i = 0; i < iloscLinii; i++) {
            System.out.print("Podaj text: ");
            String text = scanner.nextLine();
            polaczonyText += text;
        }

        System.out.println("Polaczony text: " + polaczonyText);
    }
}
