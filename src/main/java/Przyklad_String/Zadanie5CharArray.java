package Przyklad_String;

import java.util.Scanner;

public class Zadanie5CharArray {
    public static void main(String[] args) {
        String text = "SDA Academy";

        StringBuilder stringBuilder = new StringBuilder();

        char[] litery = text.toCharArray();
        for (int i = litery.length - 1; i >= 0; i--) {
            stringBuilder.append(litery[i]);
        }

        System.out.println("Odwrocone: " + stringBuilder.toString());
    }
}
