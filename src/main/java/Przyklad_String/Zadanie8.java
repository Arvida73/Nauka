package Przyklad_String;

import java.util.Scanner;

public class Zadanie8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj nazwe symbolu: ");
        String symbol = scanner.nextLine();

        switch (symbol) {
            case "gwiazdka":
                System.out.println("*");
                break;
            case "hash":
                System.out.println("#");
                break;
            default:
                System.out.println("Symbol  '" + symbol + "' nieznany");
        }
    }
}
