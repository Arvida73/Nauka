package Przyklad_String;

public class Zadanie5Char {
    public static void main(String[] args) {
        String text = "SDA Academy";

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = text.length() - 1; i >= 0; i--) {
            stringBuilder.append(text.charAt(i));
        }

        System.out.println("Odwrocone: " + stringBuilder.toString());
    }
}
