package Przyklad_String;

import java.util.Scanner;

public class Zadanie4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj ilosc liczb: ");
        int iloscLiczb = scanner.nextInt();

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < iloscLiczb; i++) {
            System.out.print("Podaj liczbe: ");
            int liczba = scanner.nextInt();
            stringBuilder.append(liczba);
        }

        System.out.println("Text: " + stringBuilder.toString());
    }
}
