package Przyklad_String;

import java.util.Scanner;

public class Zadanie1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj text: ");
        String text = scanner.nextLine();

        System.out.println("Dlugosc: " + text.length());
    }
}
