package Przyklad_String;

public class Zadanie6 {
    public static void main(String[] args) {
        // Pomijamy spacje, znaki specjalne, cyfry itd....
        String text = "priorytetowy";

        int iloscSamoglosek = 0;
        int iloscSpolglosek = 0;
        for (char litera : text.toCharArray()) {
            switch (litera) {
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                case 'y':
                    iloscSamoglosek++;
                    break;
                default:
                    iloscSpolglosek++;

            }
        }
        System.out.println("Ilosc samoglosek: " + iloscSamoglosek);
        System.out.println("Ilosc spolglosek: " + iloscSpolglosek);
    }
}
