package Przyklad_WzorceProjektowe3;

public class Hill extends Terrain {
    public Hill() {
        super("Wzgorze", 20);
    }

    public Integer getFuelCost() {
        return 20;
    }
}
