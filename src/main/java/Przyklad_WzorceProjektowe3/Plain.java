package Przyklad_WzorceProjektowe3;

public class Plain extends Terrain {
    public Plain() {
        super("Rownina", 10);
    }

    public Integer getFuelCost() {
        return 10;
    }
}
