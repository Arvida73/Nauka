package Przyklad_Mapy;

import Przyklad_Kolekcje2.Person;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PersonMapTest {
    // alt + cmd + m = exctract method

    public static void main(String[] args) {
        getputs();
        HashMap<Person, List<Person>> klasy = new HashMap<>();
        Person nauczyciel = new Person("Ola","Danuta",30);

        Person uczen1 = new Person("Imie1","nazwisko1",1);
        Person uczen2 = new Person("Imie2","nazwisko2",2);
        Person uczen3 = new Person("Imie3","nazwisko3",3);
        Person uczen4 = new Person("Imie4","nazwisko4",4);

        List<Person> uczniowie = new ArrayList<>();
        uczniowie.add(uczen1);
        uczniowie.add(uczen2);
        uczniowie.add(uczen3);
        uczniowie.add(uczen4);

        klasy.put(nauczyciel,uczniowie);
        List<Person> listFromMap = klasy.get(new Person("Anna","MEE",30));
        System.out.println(listFromMap);


    }

    private static void getputs() {
        HashMap<Integer, Person> dziennik = new HashMap<>();
        Person p1 = new Person("Hanna", "Mostowiak", 4);
        Person p2 = new Person("Ania", "Kowal", 4);

        dziennik.put(1, p1);
        dziennik.put(2, p2);
        dziennik.put(4, p1);
        dziennik.put(2,p2);


        Person p3 = dziennik.get(3);
        System.out.println(p3);

        Person p4 = dziennik.get(4);
        System.out.println(p4);
    }
}
