package Przyklad_Klasy_Abstract;

public abstract class Dog extends Animal {
// interefejsy nie moga rozszezac klas

    public Dog(int age) {
        super(age);
        System.out.println("Tworze psa");
    }

    public int getAge() { return  age; }
    }
