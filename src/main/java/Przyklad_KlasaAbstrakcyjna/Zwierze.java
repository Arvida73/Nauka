package Przyklad_KlasaAbstrakcyjna;

public abstract class Zwierze {
    private final int wiek;

    public Zwierze(int wiek) {
        this.wiek = wiek;
    }

    public abstract void dajGlos();
}
