package Przyklad_KlasaAbstrakcyjna;

public class Kot extends Zwierze {

    public Kot(int wiek) {
        super(wiek);
    }

    @Override
    public void dajGlos() {
        System.out.println("Kot - glos");
    }
}
