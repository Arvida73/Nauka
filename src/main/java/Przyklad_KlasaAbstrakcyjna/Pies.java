package Przyklad_KlasaAbstrakcyjna;

public class Pies extends Zwierze {

    public Pies(int wiek) {
        super(wiek);
    }

    @Override
    public void dajGlos() {
        System.out.println("Pies - glos");
    }
}
