package Przyklad_Platnosc;

public class User {
    private String name;
    private int age;
    private String sex;
    private int paidAmount;

    public User(String name) {
        this.name = name;
    }

    public User(String name,int age) {
        this.name = name;
        this.age = age;

    }

    // gettery i settery
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public int getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(int paidAmount) {
        this.paidAmount = paidAmount;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public boolean coursePaid() {
        return paidAmount > 100_000;
    }
}