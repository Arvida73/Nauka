package Przyklad_Metody;

public class Zadanie6 {
    public static void main(String[] args) {
        Zadanie6 zadanie = new Zadanie6();

        int dlugosc = zadanie.computeLength("SDA Academy");

        System.out.println("Dlugosc: " + dlugosc);
    }

    private int computeLength(String string) {
        return string.length();
    }

}
