package Przyklad_Metody;

public class Zadanie2 {
    public static void main(String[] args) {
        int liczba1 = 5;
        int liczba2 = 15;
        Zadanie2 zadanie = new Zadanie2();
        int suma = zadanie.sum(liczba1, liczba2);

        System.out.println(liczba1 + "+" + liczba2 + "=" + suma);
        System.out.println(liczba1 + "-" + liczba2 + "=" + zadanie.subtract(liczba1, liczba2));
        System.out.println(liczba1 + "*" + liczba2 + "=" + zadanie.multiply(liczba1, liczba2));
        System.out.println(liczba1 + "+" + liczba2 + "=" + zadanie.divide(liczba1, liczba2));
    }

    private int sum(int liczba1, int liczba2) {
        return liczba1 + liczba2;
    }

    private int subtract(int liczba1, int liczba2) {
        return liczba1 - liczba2;
    }

    private int multiply(int liczba1, int liczba2) {
        return liczba1 * liczba2;
    }

    private double divide(int liczba1, int liczba2) {
        return (double)liczba1 / liczba2;
    }
}
