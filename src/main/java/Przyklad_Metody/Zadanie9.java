package Przyklad_Metody;

public class Zadanie9 {
    public static void main(String[] args) {
        Zadanie9 zadanie = new Zadanie9();

        boolean czyTrojkat111 = zadanie.czyDaSieZbudowacTrojkat(1, 2, 3);
        System.out.println("Czy z bokow 1,1,1 da sie zbudowac trojkat: " + czyTrojkat111);

        boolean czyTrojkat345 = zadanie.czyDaSieZbudowacTrojkat(3, 4, 5);
        System.out.println("Czy z bokow 3,4,5 da sie zbudowac trojkat: " + czyTrojkat345);
    }

    private boolean czyDaSieZbudowacTrojkat(double bok1, double bok2, double bok3) {
        return bok1 + bok2 > bok3
                && bok1 + bok3 > bok2
                && bok2 + bok3 > bok1;
    }
}
