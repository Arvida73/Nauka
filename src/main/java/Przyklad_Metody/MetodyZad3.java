package Przyklad_Metody;

public class MetodyZad3 {
    public static void main(String[] args) {
        int liczbaA = 50;
        int liczbaB = 2;
        int suma = liczbaA + liczbaB;
        int roznica  = liczbaA - liczbaB;
        int iloczyn = liczbaA * liczbaB;
        System.out.println("Suma liczb " + liczbaA +" + "+  liczbaB + " = " + suma);
        System.out.println("Roznica liczb "+liczbaA +" - "+ liczbaB + " = " + roznica);
        System.out.println("Iloczyn liczb " + liczbaA + " * " + liczbaB +" = " + iloczyn);
    }
}
