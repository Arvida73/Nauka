package Przyklad_Metody;

public class Zadanie8 {
    public static void main(String[] args) {
        Zadanie8 zadanie = new Zadanie8();

        boolean parzysta10 = zadanie.czyParzysta(10);
        System.out.println("Liczba 10 jest parzysta: " + parzysta10);

        boolean parzysta11 = zadanie.czyParzysta(11);
        System.out.println("Liczba 11 jest parzysta: " + parzysta11);
    }

    private boolean czyParzysta(int liczba) {
        return liczba % 2 == 0;
    }
}
