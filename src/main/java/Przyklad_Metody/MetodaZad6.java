package Przyklad_Metody;

import java.util.Scanner;

public class MetodaZad6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe");
        int liczba = scanner.nextInt();
        int potega = (int) Math.pow(liczba,3);
        System.out.println("Liczba " + liczba +" podniesiona do potegi 3 = "+potega);
    }
}
