package Przyklad_Metody;

public class Zadanie1 {
    public static void main(String[] args) {
        int liczba1 = 5;
        int liczba2 = 15;
        int suma = new Zadanie1().sum(liczba1, liczba2);

        System.out.println(liczba1 + "+" + liczba2 + "=" + suma);
    }

    private int sum(int liczba1, int liczba2) {
        return liczba1 + liczba2;
    }
}
