package Przyklad_Metody;

public class Zadanie7 {
    public static void main(String[] args) {
        Zadanie7 zadanie = new Zadanie7();

        int potegaPetla = zadanie.obliczPotegePetla(2, 10);
        int potegaPetlaZero = zadanie.obliczPotegePetla(2, 0);

        System.out.println("Wynik potegowania 2 ^ 10 petla: " + potegaPetla);
        System.out.println("Wynik potegowania 2 ^ 0 petla: " + potegaPetlaZero);

        int potegaBiblioteka = zadanie.obliczPotegeBiblioteka(2, 10);
        int potegaBibliotekaZero = zadanie.obliczPotegeBiblioteka(2, 0);

        System.out.println("Wynik potegowania 2 ^ 10 biblioteka: " + potegaBiblioteka);
        System.out.println("Wynik potegowania 2 ^ 0 biblioteka: " + potegaBibliotekaZero);
    }

    private int obliczPotegePetla(int podstawa, int wykladnik) {
        if (wykladnik == 0) {
            return 1;
        }
        int potega = 1;
        for (int i = 0; i < wykladnik; i++) {
            potega *= podstawa;
        }
        return potega;
    }

    private int obliczPotegeBiblioteka(int podstawa, int wykladnik) {
        return (int)Math.pow(podstawa, wykladnik);
    }
}
