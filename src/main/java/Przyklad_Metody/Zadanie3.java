package Przyklad_Metody;

public class Zadanie3 {
    public static void main(String[] args) {
        int liczba1 = 5;
        int liczba2 = 0;
        Zadanie3 zadanie = new Zadanie3();

        double iloraz = zadanie.divide(liczba1, liczba2);
        System.out.println(liczba1 + "/" + liczba2 + "=" + iloraz);
    }

    private int sum(int liczba1, int liczba2) {
        return liczba1 + liczba2;
    }

    private int subtract(int liczba1, int liczba2) {
        return liczba1 - liczba2;
    }

    private int multiply(int liczba1, int liczba2) {
        return liczba1 * liczba2;
    }

    private double divide(int dzielna, int dzielnik) {
        if (dzielnik == 0) {
            System.out.println("Nie wolno dzielic przez 0");
            return -1;
        }
        return (double)dzielna / dzielnik;
    }
}
