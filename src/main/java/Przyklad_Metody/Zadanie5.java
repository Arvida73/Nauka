package Przyklad_Metody;

public class Zadanie5 {
    public static void main(String[] args) {
        Zadanie5 zadanie = new Zadanie5();

        int[] tablica = {1, 2, 3};

        zadanie.printMinMax(tablica);
    }

    private void printMinMax(int[] array) {
        int najmniejsza = Integer.MAX_VALUE;
        int najwieksza = Integer.MIN_VALUE;

        for (int element : array) {
            if (element < najmniejsza) {
                najmniejsza = element;
            }
            if (element > najwieksza) {
                najwieksza = element;
            }
        }

        System.out.println("Najwieksza wartosc: " + najwieksza);
        System.out.println("Najmniejsza wartosc: " + najmniejsza);
    }
}
