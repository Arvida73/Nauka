package Przyklad_Metody;

public class Zadanie4 {
    public static void main(String[] args) {
        Zadanie4 zadanie = new Zadanie4();

        int[] tablica = {1, 2, 3};

        int suma = zadanie.sumArray(tablica);

        System.out.println("Suma: " + suma);
    }

    private int sumArray(int[] array) {
        int suma = 0;
        for (int element : array) {
            suma += element;
        }
        return suma;
    }
}
