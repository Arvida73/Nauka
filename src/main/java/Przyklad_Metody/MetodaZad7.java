package Przyklad_Metody;

import java.util.Scanner;

public class MetodaZad7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe ");
        int liczba = scanner.nextInt();
        int pierwiastek = (int) Math.sqrt(liczba);
        System.out.println("Pierwiastek z liczby " + liczba + " = "+pierwiastek);
    }
}
