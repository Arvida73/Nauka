package Przyklad_Polimorfizm;

public class OdtwarzaczBlueRay implements OdtwarzaczFilmow {
    @Override
    public void odtwarzaj(String film) {
        System.out.println("Odtwarzam blue ray: " + film);
    }
}
