package Przyklad_Polimorfizm;

public class OdtwarzaczCD implements OdtwarzaczFilmow {
    @Override
    public void odtwarzaj(String film) {
        System.out.println("Odtwarzam CD: " + film);
    }
}
