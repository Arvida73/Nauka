package Przyklad_Polimorfizm;

public class OdtwarzaczDVD implements OdtwarzaczFilmow {
    @Override
    public void odtwarzaj(String film) {
        System.out.println("Odtwarzam DVD: " + film);
    }

    @Override
    public void stop() {
        System.out.println("Odtwarzacz DVD - stop");
    }
}
