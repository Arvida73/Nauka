package Przyklad_Polimorfizm;

public interface OdtwarzaczFilmow {
    void odtwarzaj(String film);

    default void stop() {
        System.out.println("Zatrzymanie odtwarzania");
    }
}
