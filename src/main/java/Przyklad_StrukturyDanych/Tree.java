package Przyklad_StrukturyDanych;

public class Tree {
    static class Wezel {
        int wartosc;
        Wezel lewy, prawy;


        Wezel(int wartosc) {
            this.wartosc = wartosc;
            lewy = null;
            prawy = null;
        }
    }

    Wezel korzen;

    public boolean znajdzElement(int liczba) {
        Wezel aktulane = korzen;
        while (aktulane != null) {
            if (aktulane.wartosc == liczba) {
                return true;
            } else if (aktulane.wartosc > liczba) {
                aktulane = aktulane.lewy;
            } else {
                aktulane = aktulane.prawy;
            }
        }
        return false;
    }

    public void setRoot(Wezel root) {
        this.korzen = root;
    }

    int minWartosc(Wezel korzen) {
        Wezel minWartosc = korzen;
        while (minWartosc.lewy != null) {
            minWartosc = minWartosc.lewy;
        }
        return (minWartosc.wartosc);
    }

//    public usuwanie1(int i) {
//        if (i == korzen.wartosc) {
//
//            return;
//        }
//        korzen tymczasowaWartosc = korzen;
//        while (tymczasowaWartosc != null) {
//            if (tymczasowaWartosc.lewy != null) {
//                if (i < tymczasowaWartosc.wartosc) {
//                    if (i == tymczasowaWartosc.lewy.wartosc) {
//                        break;
//                        ;
//                    } else {
//                        tymczasowaWartosc = tymczasowaWartosc.lewy;
//                    }
//
//                }
//            } else if (tymczasowaWartosc.prawy != null) {
//                if (i > tymczasowaWartosc.wartosc) {
//                    if (i == tymczasowaWartosc.prawy.wartosc) {
//                        break;
//                        ;
//                    } else {
//                        tymczasowaWartosc = tymczasowaWartosc.prawy;
//                    }
//                }
//            } else {
//                return;
//            }
//        }
//        if (tymczasowaWartosc.prawy != null) {
//            korzen tymczasowa = tymczasowaWartosc.prawy;
//            while (tymczasowa.lewy != null) {
//                tymczasowa = tymczasowa.lewy;
//            }
//            if(i<tymczasowaWartosc.wartosc){
//                tymczasowaWartosc.lewy=tymczasowa.lewy;
//            }else {
//                tymczasowaWartosc.prawy= tymczasowa.lewy;
//            }
//        } else if (tymczasowaWartosc.lewy != null) {// czy istnieje
//            korzen tymczasowa = tymczasowaWartosc.lewy;
//            while (tymczasowa.prawy != null) {
//                tymczasowa = tymczasowa.prawy;
//            }
//        }
//    }
//    public boolean usuwanie(int usuwanieWezla) {
//        Wezel rodzic = korzen;
//        Wezel aktualne = korzen;
//        boolean leweDziecko = false;
//        while (aktualne.wartosc! = usuwanieWezla){
//           rodzic=aktualne;
//            if (aktualne.wartosc > usuwanieWezla) {
//                leweDziecko=true;
//                aktualne=aktualne.lewy;
//            }else {
//                leweDziecko=false;
//                aktualne=aktualne.prawy;
//            }
//            if(aktualne==null){
//                return false;
//            }
//            }
//        if (aktualne.lewy == null && aktualne.prawy == null) {
//            if(aktualne==korzen){
//                korzen=null;
//            }
//        }
//    }
    public void wkladamy(Wezel wezel, int wartosc) {

        if (wartosc < wezel.wartosc) {
            if (wezel.lewy != null) {
                wkladamy(wezel.lewy, wartosc);
            } else System.out.println("Wkładamy " + wartosc + " dajemy na lewo od " + wezel.wartosc);
            wezel.lewy = new Wezel(wartosc);

        } else if (wartosc > wezel.wartosc) {
            if (wezel.prawy != null) {
                wkladamy(wezel.prawy, wartosc);
            } else System.out.println("Wkładamy " + wartosc + " dajemy na prawo od " + wezel.wartosc);
            wezel.prawy = new Wezel(wartosc);
        }
    }

    public void kolejnosc(Wezel wezel) {
        if (wezel != null) {
            kolejnosc(wezel.lewy);
            System.out.println("." + wezel.wartosc);
            kolejnosc(wezel.prawy);

        }
    }

    public void przechodzenie(Wezel wezel) {
        if (wezel != null) {
            System.out.print(" " + wezel.wartosc);
            przechodzenie(wezel.lewy);
            przechodzenie(wezel.prawy);
        }
    }

    public static void main(String[] args) {
        Tree drzewo = new Tree();
        Wezel korzen = new Wezel(5);
        System.out.println("Przykladowe drzewo binarne");
        System.out.println("");
        System.out.println("Budujemy drzewo  || Liczba pierwsza : " + korzen.wartosc);
        drzewo.wkladamy(korzen, 2);
        drzewo.wkladamy(korzen, 5);
        drzewo.wkladamy(korzen, 6);
        drzewo.wkladamy(korzen, 7);
        drzewo.wkladamy(korzen, 9);
        drzewo.wkladamy(korzen, 3);
        drzewo.wkladamy(korzen, 4);
        System.out.println("CALY KORZEŃ ");
        drzewo.przechodzenie(korzen);
        System.out.println(drzewo.minWartosc(korzen));
    }
}
