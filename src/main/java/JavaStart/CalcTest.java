package JavaStart;

import java.util.Calendar;

public class CalcTest {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        double a = 5;
        double b = 10;
        double c = 2;
        System.out.println("A: " + a + " B: "+b+" C: "+c);
        System.out.println("A+B= "+ calculator.add(a,b));
        System.out.println("A+B+C= "+ calculator.add(a,b,c));

        System.out.println("A-B= " + calculator.substract(a,b));
        System.out.println("A-B-C= " + calculator.substract(a,b,c));
    }


}
class Calculator{
    double add(double a,double b){
        return a+b;
    }
    double add(double a,double b,double c){
        return add(a, b)+c;
    }
    double substract(double a,double b){
        return a-b;
    }
    double substract(double a,double b,double c){
        return substract(a, b)-c;
    }


}
