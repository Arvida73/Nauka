package JavaStart;

public class KonweretJednostekMetryczncyh {
    public static void main(String[] args) {
        Convert convert = new Convert();
        double m = 2.5;
        double mToCM =convert.convertMtoCm(m);
        double mToMM = convert.convertMtoMM(m);
        System.out.println(m+ " m " + " to " + mToCM + " cm i " + mToMM + " mm ");

    }

}

class Convert {
    double convertMtoCm(double meters){
        return meters*100;
    }
    double convertMtoMM(double meters){
        return meters*1000;
    }
    double convertCMtoM(double cm){
        return cm/100;
    }
    double convertMMtoM(double mm){
        return mm/1000;
    }

}





