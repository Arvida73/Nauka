package JavaStart;

public class Company {
    public static void main(String[] args) {
        Employee emp = new Employee();
        emp.firstName="Jan";
        emp.lastName="Kowalski";
        emp.salary=5000;
        emp.bonus=500;
        emp.printTotalSalary();
    }

}
class Employee {
    String firstName;
    String lastName;
    double salary;
    double bonus;

    void printTotalSalary() {
        double totalSalary = this.salary + this.bonus;
        String message = " Calkowita wyplata: " + totalSalary;
        System.out.println(message);
    }
}