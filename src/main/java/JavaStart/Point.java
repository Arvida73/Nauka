package JavaStart;

public class Point {
    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Point() {
    }

    public Point(int x, int y) {
        setX(x);
        setY(y);
    }
}

class PointController {
    public void addX(Point p) {
        p.setX(p.getX() + 1);
    }

    public void minusX(Point p) {
        p.setX(p.getX() - 1);
    }

    public void addY(Point p) {
        p.setY(p.getY() + 1);
    }

    public void minusY(Point p) {
        p.setY(p.getY() - 1);
    }
}

class PointApplication {
    public static void main(String[] args) {
        Point p1 = new Point(10, 20);
        PointController pc = new PointController();

        final int addX = 0;
        final int addY = 1;
        final int minusX = 2;
        final int minusY = 3;

        int option = addX;
        switch (option) {
            case addX:
                pc.addX(p1);
                break;
            case addY:
                pc.addY(p1);
                break;
            case minusX:
                pc.minusX(p1);
                break;
            case minusY:
                pc.minusY(p1);
                break;
            default:
                System.out.println("Podano błędną wartość");
        }

        System.out.println("Punkt po zmianie: (" + p1.getX() + ";" + p1.getY() + ")");
    }
}
//        pc.addX(p1);
//        System.out.println("Punkt addX: (" + p1.getX()+";"+p1.getY()+")");
//        pc.addY(p1);
//        System.out.println("Punkt addY: (" + p1.getX()+";"+p1.getY()+")");
//        pc.minusX(p1);
//        System.out.println("Punkt minusX: (" + p1.getX()+";"+p1.getY()+")");
//        pc.minusY(p1);
//        System.out.println("Punkt minusY: (" + p1.getX()+";"+p1.getY()+")");



