package JavaStart;

public class Konwertergodzin {
    public static void main(String[] args) {
        Converts convert = new Converts();

        int hours = 1;
        int minutes = convert.hoursToMin(hours);
        int second = convert.minutesToSec(minutes);
        int milisec = convert.secondToMilSec(second);

        System.out.println(hours + " godzin to "+minutes+" minut a "+second+ " secund a " + milisec+" milisec ");
    }
}
class Converts{
    int hoursToMin(int hours){
        return hours*60;
    }
    int minutesToSec(int minutes){
        return minutes*60;
    }
    int secondToMilSec(int second){
        return second*1000;
    }

}
