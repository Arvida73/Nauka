package JavaStart;

import java.util.Scanner;

public class LoopWhile {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Ile liczb chcesz zsumowac?");
        int numbers = reader.nextInt();

        int sum = 0;
        while (numbers-- > 0) {
            System.out.println("Podaj kolejna liczbe");
            sum = sum + reader.nextInt();

        }
        System.out.println("suma"+sum);
        reader.close();
    }
}
