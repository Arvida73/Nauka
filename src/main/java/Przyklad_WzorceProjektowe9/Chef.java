package Przyklad_WzorceProjektowe9;

public abstract class Chef {
    protected String name;

    public void cook(){
        System.out.println(name + "nastawia wode");
        boilEgg();
        System.out.println(name + "serwuje jajko gosciom");
    }
    public  abstract void boilEgg();
}
