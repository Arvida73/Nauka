package Przyklad_WzorceProjektowe9;

public class SoftChef extends Chef {

    public SoftChef(String name) {
        this.name = name;
    }

    public void boilEgg(){

        System.out.println(name + " gotuje jajka na twardo");

    }
}
