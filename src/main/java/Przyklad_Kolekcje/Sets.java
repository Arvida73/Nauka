package Przyklad_Kolekcje;

import com.sun.istack.internal.NotNull;

import java.util.Set;
import java.util.TreeSet;

public class Sets {
    public static void main(String[] args) {
        Set<Integer> numbers = new TreeSet<>(); //(Comparator.reverseOrder());
        numbers.add(1);
        numbers.add(5);

        for ( int i : numbers){
            System.out.println(i);
        }
        numbers.remove(1);
        for(int i : numbers){
            System.out.println(i);
        }
        Set<ClassToKeepInSet> customsets = new TreeSet<>();
        customsets.add(new ClassToKeepInSet(1,"a"));
        customsets.add(new ClassToKeepInSet(2,"y"));
        for(ClassToKeepInSet element : customsets){
            System.out.println(element);

        }
     //   ClassToKeepInSet[] customTable = new ClassToKeepInSet[customsets.size()];
      //  for(Iterator<ClassToKeepInSet> iter; ; iter.hasNext());
    }
}
class  ClassToKeepInSet  {
    int intValue;
    String stringValue;

    public ClassToKeepInSet(int intValue, String stringValue) {
        this.intValue = intValue; //implements Comparable<ClassToKeepInSet>
        this.stringValue = stringValue;
    }
    public int compareTo(@NotNull ClassToKeepInSet classToKeepInSet) {
        if ( intValue <classToKeepInSet.intValue){
            return -1;
        } else if (intValue > classToKeepInSet.intValue) {
            return 1;
        } else {
            return 0;
        }


    }
    @Override
    public String toString() {
        return "ClassToKeepInSet{" +
                "intValue=" + intValue +
                ", stringValue='" + stringValue + '\'' +
                '}';
    }
}