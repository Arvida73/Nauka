package Przyklad_Sortowanie;

public class Fibonacciego {
    public static void main(String[] args) {
        Fibonacciego fibonacciego = new Fibonacciego();
        int fibInte,fibRek = 0;
        fibInte=fibonacciego.Fibliteracja(42);
        fibRek=fibonacciego.FibRekurencja(42);
        System.out.println(fibInte);
    }

    public int Fibliteracja(int n) {

        int pierwszy = 0;
        int drugi = 1;
        int kolejny = 0;
        for (int i = 2; i < n; i++) {
            kolejny = pierwszy + drugi;
            pierwszy = drugi;
            drugi = kolejny;

        }
        return kolejny;
    }
    public int FibRekurencja(int n) {
        return n<2? n : FibRekurencja(n - 1) + FibRekurencja(n - 2);
    }
}
