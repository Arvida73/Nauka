package Przyklad_Sortowanie;

import java.security.SecureRandom;
import java.util.Arrays;

public class SortowaniePrzezWstawianie {
    // Sortowanie tablicy algorytmem sortowania przez wstawianie
    public static void insertionSort(int[] data) {
        // Przejście w pętli przez data.length - 1 elementów
        for (int next = 1; next < data.length; next++) {
            int insert = data[next]; // Wartość do wstawienia
            int moveItem = next; // Lokalizacja do wstawienia elementu

            // Poszukaj miejsca wstawienia aktualnego elementu
            while (moveItem > 0 && data[moveItem - 1] > insert) {
                // Przesuń element w prawo o jedno miejsce
                data[moveItem] = data[moveItem - 1];
                moveItem--;
            }

            data[moveItem] = insert; // Umieść wstawiany element
            printPass(data, next, moveItem); // Wyświetl przebieg algorytmu
        }
    }

    // Wyświetl przebieg algorytmu
    public static void printPass(int[] data, int pass, int index) {
        System.out.printf("Po przebiegu %2d: ", pass);

        // Wyświetlaj elementy aż do wybranego
        for (int i = 0; i < index; i++) {
            System.out.printf("%d ", data[i]);
        }

        System.out.printf("%d* ", data[index]); // Wskaż zamianę

        // Wyświetl pozostałą część
        for (int i = index + 1; i < data.length; i++) {
            System.out.printf("%d ", data[i]);
        }

        System.out.printf("%n "); // Wyrównanie

        // Wskaż posortowaną część tablicy
        for (int i = 0; i <= pass; i++) {
            System.out.print("-- ");
        }
        System.out.println();
    }


    public static void main(String[] args) {
        SecureRandom generator = new SecureRandom();

        // Utwórz nieposortowaną tablicę 10 liczb losowych
        int[] data = generator.ints(10, 10, 100).toArray();

        System.out.printf("Tablica nieposortowana: %s%n%n", Arrays.toString(data));
        insertionSort(data); // Posortuj tablicę
        System.out.printf("%nTablica posortowana: %s%n", Arrays.toString(data));
    }
}

