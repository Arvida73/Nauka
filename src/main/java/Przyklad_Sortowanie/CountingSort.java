package Przyklad_Sortowanie;

class CountingSort {
    void sort(char arr[]) {
        int n = arr.length;

        // Wyjściowa tablica znaków, która posortuje arr
        char output[] = new char[n];

        // Utwórz tablicę liczników do przechowywania liczby inidividul
        // znaki i zainicjuj tablicę liczników jako 0
        int count[] = new int[256];
        for (int i = 0; i < 256; ++i)
            count[i] = 0;

        // przechowuj liczbę każdego znaku
        for (int i = 0; i < n; ++i)
            ++count[arr[i]];


        // Zmień liczbę [i], aby liczba [i] zawierała teraz wartość rzeczywistą
        //pozycja tego znaku w tablicy wyjściowej
        for (int i = 1; i <= 255; ++i)
            count[i] += count[i - 1];

        //Zbuduj wyjściową tablicę znaków
        // Aby zapewnić stabilność, działamy w odwrotnej kolejności.
        for (int i = n - 1; i >= 0; i--) {
            output[count[arr[i]] - 1] = arr[i];
            --count[arr[i]];
        }

        // Skopiuj tablicę wyjściową do arr, aby teraz arr
        // zawiera posortowane znaki
        for (int i = 0; i < n; ++i)
            arr[i] = output[i];
    }

    public static void main(String args[]) {
        CountingSort ob = new CountingSort();
        char arr[] = {'g', 'e', 'e', 'k', 's', 'f', 'o',
                'r', 'g', 'e', 'e', 'k', 's'
        };

        ob.sort(arr);

        System.out.print("Sorted character array is ");
        for (int i = 0; i < arr.length; ++i)
            System.out.print(arr[i]);
    }
}