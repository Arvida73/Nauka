package Przyklad_Sortowanie;

import java.util.Arrays;

public class SortowanieBabelkowe {
    public static void main(String[] args) {
        int tab[] = {1, 9, 2, 3, 4, 5, 10, 12, 1, 3};
        System.out.println("Zawartość tablicy : " + Arrays.toString(tab));
        for (int a = 0; a < tab.length ;a++) {
            for (int j = 0; j < tab.length - 1; j++)
                if (tab[j] > tab[j + 1]) {
                    int temp = tab[j];
                    tab[j] = tab[j + 1];
                    tab[j + 1] = temp;

                }
        }
        System.out.println("Tablica posortowana : " + Arrays.toString(tab));
    }

}
