package Przyklad_Sortowanie;

import java.util.Arrays;

public class SortowaniePrzezWybieranie {
    public static void main(String[] args) {
        int sort[] = {1, 2, 5, 1, 10, 1};
        int najmniejsza = 0;
        for (int i = 0; i < sort.length; i++) {
            najmniejsza = i;
            for (int j = i; j < sort.length; j++) {
                if (sort[j] < sort[najmniejsza]) {
                    najmniejsza = j;
                }

            }
            int temp = sort[najmniejsza];
            sort[najmniejsza] = sort[i];
            sort[i] = temp;

        }
        System.out.println("Tablica posortowana : " + Arrays.toString(sort));
    }
}