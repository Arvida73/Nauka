package Przyklad_TypyGeneryczne2;

public class Triple <THIRD_TYPE,FOURTH_TYPE,FIFTH_TYPE> {
    THIRD_TYPE third;
    FOURTH_TYPE fourth;
    FIFTH_TYPE fifth;

    public Triple(THIRD_TYPE third, FOURTH_TYPE fourth, FIFTH_TYPE fifth) {
        this.third = third;
        this.fourth = fourth;
        this.fifth = fifth;
    }

    public THIRD_TYPE getThird() {
        return third;
    }

    public FOURTH_TYPE getFourth() {
        return fourth;
    }

    public FIFTH_TYPE getFifth() {
        return fifth;
    }
}