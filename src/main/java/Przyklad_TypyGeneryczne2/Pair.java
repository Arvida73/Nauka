package Przyklad_TypyGeneryczne2;
public class Pair <FIRSTS_TYPE,SECOND_TYPE> {
    FIRSTS_TYPE First;
    SECOND_TYPE Second;

    public Pair(FIRSTS_TYPE first, SECOND_TYPE second) {
        First = first;
        Second = second;
    }

    public FIRSTS_TYPE getFirst() {
        return First;
    }

    public SECOND_TYPE getSecond() {
        return Second;
    }
}