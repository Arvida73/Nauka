package Przyklad_TypyGeneryczne2;

public class MainPair {
    public static void main(String[] args) {
        Pair<String,Integer> pair = new  Pair<>("StringValue",5);
        String firstValue = pair.getFirst();
        Integer secondValue = pair.getSecond();

        System.out.println(firstValue + " : " + secondValue);
    }
}
