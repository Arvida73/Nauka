package Przyklad_Stream4;


import java.util.ArrayList;
import java.util.List;

public class Album {
   public final String nameOfBand;
    public  final long costOfTracks;
    private List<Track> tracks;

  private  List<Track> trackList = new ArrayList<Track>();


    public Album(String nameOfBand, long costOfTracks,List<Track> trackList) {
        this.nameOfBand = nameOfBand;
        this.costOfTracks = costOfTracks;
        this.trackList = trackList;
    }
    public void addTrack(Track track) {
        tracks.add(track);
    }

    public String getNameOfBand() {
        return nameOfBand;
    }

    public long getCostOfTracks() {
        return costOfTracks;
    }

    @Override
    public String toString() {
        return "Album{" +
                "nameOfBand='" + nameOfBand + '\'' +
                ", costOfTracks=" + costOfTracks +
                ", tracks=" + tracks +
                ", trackList=" + trackList +
                '}';
    }

}
