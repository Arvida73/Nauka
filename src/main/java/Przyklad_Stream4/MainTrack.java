package Przyklad_Stream4;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MainTrack {
    public static void main(String[] args) {
        List<Album> albums = getAlbums();
        albums.stream()
                .filter(x-> x.getNameOfBand().startsWith("t")
                        || x.getNameOfBand().startsWith("r"))
                .collect(Collectors.toSet())
                .forEach(x -> System.out.println(x));
    }

        private static List<Album> getAlbums() {
        List<Album> albums = new ArrayList<>();
        List<Track> tracksToAlbum = new ArrayList<>();
        tracksToAlbum.add(new Track("tr1",40));
        tracksToAlbum.add(new Track("tr2",120));
        tracksToAlbum.add(new Track("tr3",50));

        Album acdcAlbum = new Album("Acdc",50,tracksToAlbum);
        albums.add(acdcAlbum);
        return albums;
        }
 //        Track track1 = new Track("Song 1", 120);
//        Track track2 = new Track("Song 2", 200);
//        Track track3 = new Track("Song 3", 100);
//
//        Track track4 = new Track("Song 1", 300);
//        Track track5 = new Track("Song 2", 90);
//        Track track6 = new Track("Song 3", 340);
//
//        Track track7 = new Track("Song 1", 310);
//        Track track8 = new Track("Song 2", 240);
//        Track track9 = new Track("Song 3", 180);
//
//
//        List<Track> trackList1 = new ArrayList<>();
//
//        trackList1.add(track1);
//        trackList1.add(track2);
//        trackList1.add(track3);
//
//        List<Track> trackList2 = new ArrayList<>();
//
//        trackList2.add(track4);
//        trackList2.add(track5);
//        trackList2.add(track6);
//
//        List<Track> trackList3 = new ArrayList<>();
//
//        trackList3.add(track7);
//        trackList3.add(track8);
//        trackList3.add(track9);
//
//        List<Album> albumList1 = new ArrayList<>();
//        List<Album> albumList2 = new ArrayList<>();
//        List<Album> albumList3 = new ArrayList<>();
//
//
//        Album band1 = new Album("Metalica", 50, trackList1);
//        Album band2 = new Album("Bon Jovi", 80, trackList2);
//        Album band3 = new Album("Eminem", 30, trackList3);
//
//
//        albumList1.add(band1);
//        albumList1.stream();
//
//        albumList2.add(band2);
//        albumList2.stream();
//
//        albumList3.add(band3);
//        albumList3.stream();
    }
