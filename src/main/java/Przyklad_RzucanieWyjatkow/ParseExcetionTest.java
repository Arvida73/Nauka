package Przyklad_RzucanieWyjatkow;

public class ParseExcetionTest {
    public static void main(String[] args) {
        String[] tries = {"12345", "121231v", "av231232"};
        int correct = 0;

        for (String str : tries) {
            try {
                correct = Integer.parseInt(str);
                System.out.println("odczytano wartość : " + correct);
            } catch (NumberFormatException e) {
                // e.printStackTrace();
                System.out.println("Nie poprawne dane");

                throw new RuntimeException("Nie udalo sie odczytac zadnej wartosci" + e.getMessage());
            }

            //    System.out.println("odczytano wartość : " + correct);

        }
    }
}
