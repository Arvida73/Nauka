package Przyklad_Tablice;

import java.util.Arrays;
import java.util.Scanner;

public class Zadanie4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj rozmiar tablicy: ");
        int rozmiar = scanner.nextInt();

        int[] tablica = new int[rozmiar];

        for (int i = 0; i < tablica.length; i++) {
            System.out.print("Podaj liczbe na pozycje " + i + ":");
            int liczba = scanner.nextInt();
            if (liczba > 5 && liczba % 2 == 0) {
                tablica[i] = liczba;
            }
        }

        System.out.println("Tablica: " + Arrays.toString(tablica));

        int suma = 0;
        int iloczyn = 1;

        for (int element : tablica) {
            suma += element;
            if (element != 0) {
                iloczyn *= element;
            }
        }

        System.out.println("Suma: " + suma);
        System.out.println("Iloczyn: " + iloczyn);
    }
}
