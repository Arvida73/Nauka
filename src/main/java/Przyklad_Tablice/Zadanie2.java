package Przyklad_Tablice;

import java.util.Arrays;
import java.util.Scanner;

public class Zadanie2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj rozmiar tablicy: ");
        int rozmiar = scanner.nextInt();

        int[] tablica = new int[rozmiar];

        for (int i = 0; i < tablica.length; i++) {
            System.out.print("Podaj liczbe na pozycje " + i + ":");
            int liczba = scanner.nextInt();
            tablica[i] = liczba;
        }

        System.out.println("Tablica: " + Arrays.toString(tablica));
    }
}
