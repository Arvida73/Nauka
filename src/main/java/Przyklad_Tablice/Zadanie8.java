package Przyklad_Tablice;

public class Zadanie8 {
    public static void main(String[] args) {
        int numerCiagu = 10;

        int[] ciag = new int[numerCiagu];

        ciag[0] = 0;
        ciag[1] = 1;
        for (int i = 2; i < ciag.length; i++) {
            ciag[i] = ciag[i - 1] + ciag[i-2];
        }

        for (int i = ciag.length - 1; i >= 0; i--) {
            System.out.println(i + 1 + " wynosi: " + ciag[i]);
        }
    }
}
