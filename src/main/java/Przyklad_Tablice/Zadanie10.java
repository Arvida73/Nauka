package Przyklad_Tablice;

import java.util.Arrays;

public class Zadanie10 {
    public static void main(String[] args) {
        int[] tablica = {9, 8, 7, 6, 5, 4, 3, 2, 1};

        System.out.println("Przed sortowaniem: " + Arrays.toString(tablica));

        for (int i = 0; i < tablica.length; i++) {
            int indexOfSmallestElement = i;
            for (int j = i + 1; j < tablica.length; j++) {
                if (tablica[j] < tablica[indexOfSmallestElement]) {
                    indexOfSmallestElement = j;
                }
            }

            int temp = tablica[indexOfSmallestElement];
            tablica[indexOfSmallestElement] = tablica[i];
            tablica[i] = temp;
        }

        System.out.println("Po sortowaniu: " + Arrays.toString(tablica));
    }
}
