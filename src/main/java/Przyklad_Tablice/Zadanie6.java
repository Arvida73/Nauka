package Przyklad_Tablice;

import java.util.Arrays;

public class Zadanie6 {
    public static void main(String[] args) {
        int[] tablica = {1, 2, 3, 4, 5, 6, 7, 8};

        System.out.println("Tablica przed: " + Arrays.toString(tablica));

        for (int i = 0; i < tablica.length/2; i++) {
            int temp = tablica[i];
            tablica[i] = tablica[tablica.length - 1 - i];
            tablica[tablica.length - 1 - i] = temp;
        }

        System.out.println("Tablica po: " + Arrays.toString(tablica));
    }
}
