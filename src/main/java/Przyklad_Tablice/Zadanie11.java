package Przyklad_Tablice;

public class Zadanie11 {
    public static void main(String[] args) {
        int[][] tablica = new int[5][5];

        for (int i = 0; i < tablica.length; i++) {
            for (int j = 0; j < tablica.length; j++) {
                if (i == j) {
                    tablica[i][j] = 1;
                } else {
                    tablica[i][j] = -1;
                }
            }
        }

        for (int i = 0; i < tablica.length; i++) {
            for (int j = 0; j < tablica.length; j++) {
                System.out.print(tablica[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }
}
