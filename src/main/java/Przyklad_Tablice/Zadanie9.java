package Przyklad_Tablice;

public class Zadanie9 {
    public static void main(String[] args) {
        int iloscLinii = 6;

        int[] poprzedniaLinia = new int[iloscLinii];
        int[] obecnaLinia = new int[iloscLinii];

        for (int i = 0; i < iloscLinii; i++) {
            for (int j = 0; j <= i; j++) {
                if (j > 0) {
                    obecnaLinia[j] = poprzedniaLinia[j] + poprzedniaLinia[j-1];
                } else {
                    obecnaLinia[j] = 1;
                }
            }
            for (int j = 0; j <= i; j++) {
                System.out.print(obecnaLinia[j]);
                System.out.print(" ");
            }
            System.out.println();
            poprzedniaLinia = obecnaLinia;
            obecnaLinia = new int[iloscLinii];
        }
    }
}
