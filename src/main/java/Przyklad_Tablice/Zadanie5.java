package Przyklad_Tablice;

public class Zadanie5 {
    public static void main(String[] args) {
        int[][] tabliczka = new int[11][11];

        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 11; j++) {
                tabliczka[i][j] = i * j;
            }
        }

        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 11; j++) {
                System.out.println(i + "x" + j + "=" + tabliczka[i][j]);
            }
        }
    }
}
