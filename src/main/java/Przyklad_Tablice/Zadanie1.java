package Przyklad_Tablice;

public class Zadanie1 {
    public static void main(String[] args) {
        int[] tablica = {1, 2, 3, 4, 5};
        int szukanaLiczba = 3;

        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] == szukanaLiczba) {
                System.out.println("Liczba jest obecna w tablicy");
            }
        }
    }
}
