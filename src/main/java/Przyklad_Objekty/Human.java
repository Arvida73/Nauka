package Przyklad_Objekty;

public class Human {
    int weight;
    int age;
    int hight;
    String name;
    String gender;

    public int getWeight() {
        return weight;
    }

    public int getAge() {
        return age;
    }

    public int getHight() {
        return hight;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }
}
