package Przyklad_Petle;

import java.util.Scanner;

public class Zadanie13 {
    public static void main(String[] args) {
        String haslo = "SDA";

        Scanner scanner = new Scanner(System.in);

        int proba = 1;
        do {
            System.out.print("Podaj haslo: ");
            String podaneHaslo = scanner.nextLine();
            if (podaneHaslo.equals(haslo)) {
                System.out.println("Haslo prawidlowe");
                break;
            } else {
                System.out.println("Halo nieprawidlowe");
            }
            ++proba;
        } while (proba <= 3);
    }
}
