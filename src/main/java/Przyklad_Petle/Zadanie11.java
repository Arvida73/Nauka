package Przyklad_Petle;

import java.util.Scanner;

public class Zadanie11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj numer liczby pierwszej: ");
        int numerLiczby = scanner.nextInt();

        int liczbyPierwszeDoTejPory = 0;

        int liczba = 2;
        while (true) {
            boolean czyPierwsza = true;
            for (int i = 2; i < liczba; i++) {
                if (liczba % i == 0) {
                    czyPierwsza = false;
                    break;
                }
            }
            if (czyPierwsza) {
                ++liczbyPierwszeDoTejPory;
            }
            if (liczbyPierwszeDoTejPory == numerLiczby) {
                break;
            }
            liczba++;
        }

        System.out.println(numerLiczby + " liczba pierwsza to: " + liczba);
    }
}
