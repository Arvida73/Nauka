package Przyklad_Petle;

public class Zadanie19 {
    public static void main(String[] args) {
        int iloscLinii = 4;
        int liczba = 1;

        for (int i = 1; i <= iloscLinii; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(liczba++);
                System.out.print(" ");
            }
            System.out.println();
        }
    }
}
