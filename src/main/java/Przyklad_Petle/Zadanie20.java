package Przyklad_Petle;

public class Zadanie20 {
    public static void main(String[] args) {
        int liczba = 12345678;

        int odwrocona = 0;

        while (liczba != 0) {
            int ostatniaCyfra = liczba % 10;
            odwrocona *= 10;
            odwrocona += ostatniaCyfra;
            liczba /= 10;
        }

        System.out.println("Odwrocona liczba: " + odwrocona);
    }
}
