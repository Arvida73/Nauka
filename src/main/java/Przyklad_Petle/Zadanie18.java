package Przyklad_Petle;

public class Zadanie18 {
    public static void main(String[] args) {
        int liczba = 12345678;

        int liczbaCyfr = 0;

        while (liczba != 0) {
            ++liczbaCyfr;
            liczba /= 10;
        }

        System.out.println("W liczbie jest " + liczbaCyfr + " cyfr");
    }
}
