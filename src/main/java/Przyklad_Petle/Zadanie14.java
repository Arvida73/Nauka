package Przyklad_Petle;

import java.util.Scanner;

public class Zadanie14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while(true) {
            System.out.print("Podaj liczbe: ");
            int liczba = scanner.nextInt();
            if (liczba == -1) {
                break;
            }
            if (liczba % 2 == 0 && liczba % 5 == 0) {
                System.out.println("Podana liczba jest podzielna przez 2 i 5");
            }
        }
    }
}
