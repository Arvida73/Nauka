package Przyklad_Petle;

import java.util.Scanner;

public class Zadanie15 {
    public static void main(String[] args) {
        int sumaWszystkich = 0;
        int iloscWszystkich = 0;
        int sumaNieparzystych = 0;
        int iloscNieparzystych = 0;
        int sumaParzystych = 0;
        int iloscParzystych = 0;

        for (int liczba = 12; liczba < 598; liczba++) {
            sumaWszystkich += liczba;
            iloscWszystkich++;

            if (liczba % 2 == 0) {
                sumaParzystych += liczba;
                ++iloscParzystych;
            } else {
                sumaNieparzystych += liczba;
                iloscNieparzystych = iloscNieparzystych + 1;
            }
        }

        double srednia = (double)sumaWszystkich/iloscWszystkich;

        System.out.println("Suma wszystkich liczb: " + sumaWszystkich);
        System.out.println("Ilosc wszystkich liczb: " + iloscWszystkich);
        System.out.println("Srednia: " + srednia);

        System.out.println("Suma parzystych liczb: " + sumaParzystych);
        System.out.println("Ilosc parzystych liczb: " + iloscParzystych);

        System.out.println("Suma nieparzystych liczb: " + sumaNieparzystych);
        System.out.println("Ilosc nieparzystych liczb: " + iloscNieparzystych);
    }
}
