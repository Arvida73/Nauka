package Przyklad_Petle;

import java.util.Scanner;

public class Zadanie9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj dlugosc boku: ");
        int dlugoscBoku = scanner.nextInt();
        for (int i = 0; i < dlugoscBoku; i++) {
            for (int j = 0; j < dlugoscBoku; j++) {
                System.out.print("#");
            }
            System.out.println();
        }
    }
}
