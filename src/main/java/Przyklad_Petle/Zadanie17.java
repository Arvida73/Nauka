package Przyklad_Petle;

public class Zadanie17 {
    public static void main(String[] args) {
        for (int linia = 4; linia > 0; linia--) {
            for (int spacje = 0; spacje < 4 - linia; spacje++) {
                System.out.print(" ");
            }
            for (int gwiazdki = 0; gwiazdki < linia * 2 - 1; gwiazdki++) {
                System.out.print("#");
            }
            System.out.println();
        }
    }
}
