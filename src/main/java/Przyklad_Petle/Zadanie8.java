package Przyklad_Petle;

public class Zadanie8 {
    public static void main(String[] args) {
        int suma = 0;
        for (int liczba = -10; liczba < 101; liczba++) {
            if (liczba % 2 != 0) {
                suma = suma + liczba;
            }
        }
        System.out.println("Suma: " + suma);
    }
}
