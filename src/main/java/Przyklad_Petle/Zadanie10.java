package Przyklad_Petle;

import java.util.Scanner;

public class Zadanie10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj podstawe: ");
        int podstawa = scanner.nextInt();

        System.out.print("Podaj wykladnik: ");
        int wykladnik = scanner.nextInt();

        if (wykladnik == 0) {
            System.out.println("Wynik potegowania " + podstawa
                                       + " do potegi " + wykladnik
                                       + " = " + 1);
        } else {
            int potega = 1;
            for (int i = 0; i < wykladnik; i++) {
                potega *= podstawa;
            }

            System.out.println("Wynik potegowania " + podstawa
                                       + " do potegi " + wykladnik
                                       + " = " + potega);
        }
    }
}
