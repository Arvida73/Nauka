package Przyklad_Petle;

import java.util.Scanner;

public class Zadanie12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj liczbe: ");
        int liczba = scanner.nextInt();

        boolean czyPierwsza = true;
        for (int i = 2; i < liczba; i++) {
            if (liczba % i == 0) {
                czyPierwsza = false;
                break;
            }
        }
        if (czyPierwsza) {
            System.out.println("Liczba " + liczba + " to liczba pierwsza");
        } else {
            System.out.println("Liczba " + liczba + " to liczba zlozona");
        }
    }
}
