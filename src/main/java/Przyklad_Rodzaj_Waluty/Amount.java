package Przyklad_Rodzaj_Waluty;

public class Amount {
    private double amount;

    // Metoda wytworcza

    public static Amount parsePolishAmount(String amount) {
        String amountWithDot = amount.replaceAll(",", ".");
        return new Amount(Double.parseDouble(amountWithDot));
    }

        public static Amount parseEnglishAmount(String amount) {
        return new Amount(Double.parseDouble(amount));

    }

    @Override
    public String toString() {
        return "Amount{" +
                "amount=" + amount +
                '}';
    }

    public Amount(double amount) {
        this.amount = amount;
    }
}
