package Przyklad_TypyGeneryczne;

public abstract  class Food {

        protected final String name;
        protected final String weight;

    public Food(String name,String weight) {

            this.name = name;
            this.weight = weight;
        }
    abstract void prepare();

}

