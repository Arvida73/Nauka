package Przyklad_TypyGeneryczne;

public class Chef<T extends Food> {


  public void prepareMeal(T foodToPrepare){
         foodToPrepare.prepare();
     }

}
