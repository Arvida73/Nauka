package Przyklad_TypyGeneryczne;

public class GenericChefTest {
    public static void main(String[] args) {
        Chef<Beef> beefChef = new Chef<>();
        Chef<Nudle> nudleChef = new Chef<>();
        Chef<Cabbage> cabbageChef = new Chef<>();

        beefChef.prepareMeal(new Beef("beef","500"));

        Chef<Food> masterChef = new Chef<>();
        masterChef.prepareMeal(new Nudle("makaron","300"));
        masterChef.prepareMeal(new Beef("Beef","500"));
        masterChef.prepareMeal(new Cabbage("cabbage","100"));

//       Food chef1 = new Nudle("Jas","91");
//       Food chef2 = new Cabbage("Malgosia","43");
//       Food chef3 = new Beef("Bartek","80");
//       Food chef4 = new Chef("Grzesiek","75");




    }
}
