package Przyklad_TypyGeneryczne;

public class FirstGeneric {

    public static void main(String[] args) {
    FirstGenericc generic = new FirstGenericc("String");

//    if(generic.getValue()instanceof String){
//
//    }
    String value = (String)generic.getValue();

    FirstGenericc<String> genericString = new FirstGenericc<>("wartosc");
    String stringValue = genericString.getValue();



    }

}

class FirstGenericc<T> {
    private T value;


    public FirstGenericc(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }
}