package Przyklad_PeselValidator;

import java.lang.reflect.Constructor;

public class Person {
    String name;
    String surName;
    int age;
    @DefeultValue(name = "patryk")
    public Person (){
        for (Constructor constructor : Person.class.getConstructors()){
            if(constructor.isAnnotationPresent(DefeultValue.class)){
                DefeultValue annotation = (DefeultValue)constructor.getAnnotation(DefeultValue.class);
            this.name= annotation.name();
                this.surName=annotation.surName();
                this.age=annotation.age();
        }
        }
//        super(); // niejawny, domyslny
//        super() lub this ()

    }

    public Person(String name) {
        this.name = name;
    }

    public Person(String name, String surName) {
        this.name = name;
        this.surName = surName;
    }

    public Person(String name, String surName, int age) {
        this.name = name;
        this.surName = surName;
        this.age = age;
    }
    static void recreatePerson(Person person){           //szukanie bledów
            person.name = "Stas";
           person = new Person("Stas");
        System.out.println(person);
    }
    public static void main(String[] args) {
        Person person = new Person("Jas");
        recreatePerson(person);
        System.out.println(person.name);

    }

}
