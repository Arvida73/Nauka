package Przyklad_PeselValidator;

import java.time.LocalDate;

public class Pesel implements  Validatable {

    private final String pesel;
    private int[] factors = new int[]{1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1};


    public Pesel(String pesel) {
        this.pesel = pesel;
    }

    // 1×a + 3×b + 7×c + 9×d + 1×e + 3×f + 7×g + 9×h + 1×i + 3×j + 1×k
    public boolean isValid() {

        int sum = 0;
        for (int i = 0; i < factors.length; i++) {
            int numericValue = Character.getNumericValue(pesel.charAt(i));
            int multipy = factors[i] * numericValue;
        }
//     boolean result =  sum%10 == 0 ? true : false;
//       int j = 10 > 5
//               ? 10 < 20
//               ? 4
//               : 3 // trojargumentowy operator
//               : 5;
        return sum % 10 == 0;
    }

  public LocalDate cnvertToLocalTime() {
      String substring = pesel.substring(0, 6);
      int month = Integer.parseInt(pesel.substring(2,4));
      int prefiYear = month < 20
              ? 1900
              :month <40
              ? 2000
              :month <60
              ? 2100
              :month <80
              ? 1800
              : 0;
      int year = Integer.parseInt(pesel.substring(0,2))  + prefiYear;
      int day = Integer.parseInt(pesel.substring(4,7));
      return LocalDate.of(year,month,day);


  }


    public String getPesel() {
        return pesel;
    }
}