package Przyklad_PeselValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target (value = ElementType.CONSTRUCTOR)
@Retention(RetentionPolicy.RUNTIME)
public @interface DefeultValue {

    String name() default "Jerzy";
    String surName() default "Nowak";
    int age() default 5;


}
