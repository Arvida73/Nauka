package Przyklad_KlasyStatyczne;

public class KalkulatorVarargsWarunek {
    private static int sum(int... liczby) {
        if (liczby.length < 2) {
            System.out.println("Dodawanie przyjmuje conajmniej 2 liczby");
            return -1;
        }
        int suma = 0;
        for (int liczba : liczby) {
            suma += liczba;
        }
        return suma;
    }

    private static int subtract(int liczba1, int liczba2) {
        return liczba1 - liczba2;
    }

    private static int multiply(int... liczby) {
        if (liczby.length < 2) {
            System.out.println("Mnozenie przyjmuje conajmniej 2 liczby");
            return -1;
        }
        int iloczyn = 1;
        for (int liczba : liczby) {
            iloczyn *= liczba;
        }
        return iloczyn;
    }

    private static double divide(int dzielna, int dzielnik) {
        if (dzielnik == 0) {
            System.out.println("Nie wolno dzielic przez 0");
            return -1;
        }
        return (double)dzielna / dzielnik;
    }
}
