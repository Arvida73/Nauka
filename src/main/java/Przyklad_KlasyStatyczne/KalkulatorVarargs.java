package Przyklad_KlasyStatyczne;

public class KalkulatorVarargs {
    private static int sum(int... liczby) {
        int suma = 0;
        for (int liczba : liczby) {
            suma += liczba;
        }
        return suma;
    }

    private static int subtract(int liczba1, int liczba2) {
        return liczba1 - liczba2;
    }

    private static int multiply(int... liczby) {
        int iloczyn = 1;
        for (int liczba : liczby) {
            iloczyn *= liczba;
        }
        return iloczyn;
    }

    private static double divide(int dzielna, int dzielnik) {
        if (dzielnik == 0) {
            System.out.println("Nie wolno dzielic przez 0");
            return -1;
        }
        return (double)dzielna / dzielnik;
    }
}
