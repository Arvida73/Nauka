package Przyklad_KlasyStatyczne;

public class Kalkulator {
    private static int sum(int liczba1, int liczba2) {
        return liczba1 + liczba2;
    }

    private static int subtract(int liczba1, int liczba2) {
        return liczba1 - liczba2;
    }

    private static int multiply(int liczba1, int liczba2) {
        return liczba1 * liczba2;
    }

    private static double divide(int dzielna, int dzielnik) {
        if (dzielnik == 0) {
            System.out.println("Nie wolno dzielic przez 0");
            return -1;
        }
        return (double)dzielna / dzielnik;
    }
}
