package Przyklad_Kolekcje2;

import com.sun.istack.internal.NotNull;

import java.util.Objects;

public class Person implements Comparable<Person> {
    private String name;
    private String surName;
    private int age;
   // private Person person;

    public Person(String name, String surName, int age) {
        this.name = name;
        this.surName = surName;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getSurName() {
        return surName;
    }

    public int getAge() {
        return age;

    }
//    public int compareTo(@NotNull Person person){
//        return Comparator.comparing(Person::getSurName)
//                .thenComparing(Person::getName)
//                .thenComparing(Person::getAge)
//                .compare((this.person);
//
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age &&
                Objects.equals(name, person.name) &&
                Objects.equals(surName, person.surName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surName, age);
    }

    @Override
    public int compareTo(@NotNull Person person) {
        return this.surName.compareTo(person.surName);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                ", age=" + age +
                '}';
    }
}