package Przyklad_Kolekcje2;

import java.util.Comparator;

public class SortPersonComparator implements Comparator<Person> {
    public  static SortPersonComparator INSTACE
            = new SortPersonComparator();

    public int compare(Person p1, Person p2) {
        return Comparator.comparing(Person::getName)
                .thenComparing(Person::getAge)
                .thenComparing(Person::getSurName)
                .compare(p1, p2);
    }


}
