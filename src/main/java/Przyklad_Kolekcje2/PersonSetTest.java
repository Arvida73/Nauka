package Przyklad_Kolekcje2;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class PersonSetTest {
    private static Object SortPersonComparator;

    public static void main(String[] args) {
        Person p1 = new Person("Adam","Kowalski",25);
        Person p2 = new Person("Jan","Nowak",35);
        Person p3 = new Person("Jan","Nowak",40);
        Person p4 = new Person("Adam","Kowalski",20);

        Set<Person> sortByName = new TreeSet<>();
        sortByName.add(p1);
        sortByName.add(p2);
        sortByName.add(p3);
        sortByName.add(p4);


        convertToArraysAndPrint(sortByName);


      //  Set<Person> sortByName = new TreeSet<>(SortPersonComparator);
        sortByName.add(p1);
        sortByName.add(p2);
        sortByName.add(p3);
        sortByName.add(p4);

        convertToArraysAndPrint(sortByName);
    }

    private static void convertToArraysAndPrint(Set<Person> sortByName) {
        Person[] peoples = sortByName.toArray(new Person[0]);
        System.out.println(Arrays.toString(peoples));
    }
}
