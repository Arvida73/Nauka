package Przyklad_Enum;

public class DayPrinter {
   String printDayBy(WeekDay weekDay) {
       // String printByDay(weekDay) {
        switch (weekDay) {

            case MONDAY:
               return "Loops";

            case TUESDAY:
                return "Arrays";

            case WEDNESDAY:
                return "Enumus";

            case THURSDAY:
                return "Beer";

            case FRIDAY:
                return "Rest";

            case SATURDAY:
                return "Classes";

            case SUNDAY:
                return "Java";

            default:
                return "null";

        }


    }
}
