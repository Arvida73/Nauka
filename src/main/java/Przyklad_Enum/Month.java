package Przyklad_Enum;

public enum Month {
    JANUARY("Styczen",1),
    FEBRUARY("Luty",2),
    MARCH("Marzec",3),; // zakonczenie enum ;

    private String monthName;
    private int monthNumber;

    public String getMonthName() {
        return monthName;
    }

    public int getMonthNumber() {
        return monthNumber;
    }

    Month(String monthName, int monthNumber) {
        this.monthName = monthName;
        this.monthNumber = monthNumber;
    }
   public static Month getMonthBy (int monthNumber) {

        for ( Month month : Month.values()){
            if (month.getMonthNumber() == monthNumber) {
                return month;
            }
        }
        return null;

    }

    public static void main(String[] args) {
        Month monthby = Month.getMonthBy(2);
        System.out.println(monthby.monthName);
    }
}
