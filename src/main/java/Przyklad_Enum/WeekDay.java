package Przyklad_Enum;

import java.util.Arrays;

public enum WeekDay {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;

    public static void main(String[] args) {
        System.out.println(WeekDay.SUNDAY.toString().length());
        System.out.println(WeekDay.values());
        System.out.println(Arrays.toString(WeekDay.values()));
    }
}
