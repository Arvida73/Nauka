package Przykad_Dziedziczenie;

public class Prostokat {

    private final int  a;
    private  final int  b;


    public Prostokat (int a, int b) {
        this.a = a;
        this.b = b;
    }
    public int Pole() {
        return a * b;
    }
    public int Obwod() {
        return 2 * a + 2 * b;
    }
    public String tostring() {
        // prostokat ( pole = pole, obwod = obwod
        return String.format("Prostokat{pole=%d, obwod=%d}",Pole(),Obwod());
    }
}
