package Przykad_Dziedziczenie;

public class Kwadrat extends Prostokat {
    public Kwadrat(int a) {
        super(a, a);
    }

    public String tostring() {
        // prostokat ( pole = pole, obwod = obwod
        return String.format("Kwadrat{pole=%d, obwod=%d}", Pole(), Obwod());
    }
}
