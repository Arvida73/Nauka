package Przyklad_LocalDate;

import java.time.DateTimeException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Time {
    public static void main(String[] args) {
        LocalTime now = LocalTime.now();
        LocalTime time1 = LocalTime.parse("05:30:30");
        LocalTime time2 = LocalTime.of(20, 30, 00);
        System.out.println("Pokaz godzinne poranna : " + time1);
        System.out.println("Pokaz godzinne wieczorna : " + time2);
        System.out.println("Pokaz aktualna godzine : " + now);

        LocalTime now1 = LocalTime.now();
        now1 = now1.plusHours(1);
      //  DateTimeFormatter format = DateTimeFormatter.ofPattern("HH","MM");
        System.out.println("Pokaz nowa godzine "+ now1);

        LocalTime now2 = LocalTime.now();
        now2 = now2.minusMinutes(10);
        System.out.println("Pokaz Aktualna godzina i odejmij 10 minut : "+now2);

        LocalTime now3 = LocalTime.now().plusHours(1);
       // time.isAfter(timeToCompare);
//        public boolean isAfter(LocalTime other) {
//            return compareTo(other) > 0;

    }
}


//time.plus(1, ChronoUnit.HOURS); time.plusHours(1);
