package Przyklad_LocalDate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Dates {
    public static void main(String[] args) {
        //wyswietlanie daty
        LocalDate actually = LocalDate.now();
        System.out.println("Aktualna data - " +actually);
        LocalDate format = LocalDate.parse("2019-12-12");
        System.out.println("Zmiana formatu na string - "+ format);
        LocalDate date = LocalDate.of(2012,12,12);
        System.out.println("Koniec swiata - " +date);
        //Zmiana daty
        LocalDate actually1 = LocalDate.now();
        actually1 = actually1.plusDays(1);
        System.out.println("Data plus 1 dzien - "+actually1 );

        LocalDate actually2 = LocalDate.now();
        actually2 = actually2.minusDays(1);
        System.out.println("Data minus 1 dzien - " +actually2);

        //poczatek dnia
        LocalDateTime max = LocalDateTime.MAX;
        LocalDateTime anylocalDate = LocalDateTime.of(2017,11,19,12,11);
        LocalDateTime todayStart = LocalDateTime.of(LocalDate.now(), LocalTime.MIN);
        System.out.println(todayStart);



    }
}
