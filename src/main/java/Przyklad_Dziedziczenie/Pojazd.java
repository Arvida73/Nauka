package Przyklad_Dziedziczenie;

public class Pojazd {
    private double maksymalnaPredkosc;
    private double aktualnaPredkosc;

    public Pojazd(double maksymalnaPredkosc) {
        this.maksymalnaPredkosc = maksymalnaPredkosc;
    }

    public double getMaksymalnaPredkosc() {
        return maksymalnaPredkosc;
    }

    public void setMaksymalnaPredkosc(double maksymalnaPredkosc) {
        this.maksymalnaPredkosc = maksymalnaPredkosc;
    }

    public double getAktualnaPredkosc() {
        return aktualnaPredkosc;
    }

    public void setAktualnaPredkosc(double aktualnaPredkosc) {
        if (aktualnaPredkosc > maksymalnaPredkosc || aktualnaPredkosc <= 0) {
            System.out.println("Niepoprawna wartosc aktualnej predkosci!");
            return;
        }
        this.aktualnaPredkosc = aktualnaPredkosc;
    }
}
