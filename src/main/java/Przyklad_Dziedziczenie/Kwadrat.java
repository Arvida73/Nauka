package Przyklad_Dziedziczenie;

public class Kwadrat extends Prostokat {

    public Kwadrat(int bok) {
        super(bok, bok);
    }

    @Override
    public String toString() {
        return String.format("Kwadrat{pole=%d, obwod=%d}",
                             pole(), obwod());
    }
}
