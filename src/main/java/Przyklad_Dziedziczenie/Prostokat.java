package Przyklad_Dziedziczenie;

public class Prostokat {
    private int dlugosc;
    private int szerokosc;

    public Prostokat(int dlugosc, int szerokosc) {
        this.dlugosc = dlugosc;
        this.szerokosc = szerokosc;
    }

    public final int pole() {
        return dlugosc * szerokosc;
    }

    public final int obwod() {
        return 2 * dlugosc + 2 * szerokosc;
    }

    @Override
    public String toString() {
        return String.format("Prostokat{pole=%d, obwod=%d}",
                             pole(), obwod());
    }
}
