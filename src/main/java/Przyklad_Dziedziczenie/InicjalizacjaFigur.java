package Przyklad_Dziedziczenie;

import java.util.Scanner;

public class InicjalizacjaFigur {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj jaka figure stworzyc (kwadrat/prostokat): ");
        String figura = scanner.nextLine();

        if (figura.equals("kwadrat")) {
            System.out.print("Podaj bok kwadratu: ");
            int bok = scanner.nextInt();
            Kwadrat kwadrat = new Kwadrat(bok);
            System.out.println(kwadrat);
        } else {
            System.out.print("Podaj dlugosc: ");
            int dlugosc = scanner.nextInt();
            System.out.print("Podaj szerokosc: ");
            int szerokosc = scanner.nextInt();
            Prostokat prostokat = new Prostokat(dlugosc, szerokosc);
            System.out.println(prostokat);
        }
    }
}
