package Przyklad_Kompozycja;

public class Komputer {
    private Procesor procesor;
    private PlytaGlowna plytaGlowna;
    private boolean wlaczony;

    public Komputer(Procesor procesor, PlytaGlowna plytaGlowna) {
        this.procesor = procesor;
        this.plytaGlowna = plytaGlowna;
    }

    public void wlacz() {
        if (czyCzesciPasuja()) {
            wlaczony = true;
        }
    }

    private boolean czyCzesciPasuja() {
        boolean intel = procesor.czyIntel() && plytaGlowna.czyObslugujeIntel();
        boolean amd = !procesor.czyIntel() && !plytaGlowna.czyObslugujeIntel();
        return intel || amd;
    }
}
