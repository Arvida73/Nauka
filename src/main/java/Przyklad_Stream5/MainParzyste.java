package Przyklad_Stream5;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainParzyste {
    public static void main(String[] args) {
        List<Streams1> parzysteStream = Arrays.asList(
                new Streams1(",",1),
                new Streams1(",",26),
                new Streams1(",",30),
                new Streams1(",",2),
                new Streams1(",",45));
        //stwórz strumień z pięcioma elementami typu String: "first"  "second"  "third"  "fourth" "fifth" z utworzonego strumienia zwróć słowa dłuższe niż 5 znaków oraz zwróć je w formacie UPPERCASE

//        Stream<String> integerStream0 = Stream.of("First","Second","Third","Fourth","Fifth");
//        List<String> integerList = integerStream0
//                .filter(string -> string.length() > 5 )
//                .map(s -> s.toUpperCase())
//                .forEach(System.out::println);



        //twórz strumień z pięcioma elementami typu Integer:  1  26  30  2  45 i zwróć liczby parzyste

         Stream<Integer> integerStream = Stream.of(1,26,30,2,45);
         List<Integer> collector1 = integerStream.filter(integer -> integer%2 == 0).collect(Collectors.toList());
        System.out.println(collector1);

        // stwórz strumień z pięcioma elementami typu Integer:  1  26  30  2  45 i wróć maksymalną liczbę

        Stream<Integer> maxValue = Stream.of(1,26,30,2,45);
        Integer maxValue1 = maxValue.max(Integer::compareTo).get();
        System.out.println(maxValue1);

        // stwórz strumień z pięcioma elementami typu Integer:  1  26  30  2  45 i zwróć listę liczb większych od 26 jako lista z String

        Stream<Integer> integerStream1 = Stream.of(1,26,30,2,45);
        List<Integer> integerList = integerStream1.filter(integer -> integer >26).collect(Collectors.toList());
        System.out.println(integerList.toString());


    }

}
