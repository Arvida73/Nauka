//package Streamss2;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//import java.util.Set;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
//public class MainString2 {
////    stwórz strumień z pięcioma elementami typu String:
////     "first"  "second"  "third"  "fourth" "fifth"
////     z utworzonego strumienia zwróć słowa dłuższe niż 5 znaków
////     oraz zwróć je w formacie UPPERCAS
//​
//    public static void main(String[] args) {
//        //uzywajac metody Stream.of
//        Stream<String> stream1 = Stream.of("first", "second", "third", "fourth", "fifth");
//​
//        String[] stringTable = {"first", "second", "third", "fourth", "fifth"};
//        Stream<String> stream2 = Stream.of(stringTable);
//​
//        //metoda stream() na obiekcie kolekcji
//        List<String> stringList = new ArrayList<>();
//        stringList.add("first");
//        stringList.add("second");
//        stringList.add("third");
//        stringList.add("fourth");
//        stringList.add("fifth");
//        Stream<String> stream3 = stringList.stream();
//​
//        Stream<String> stream4 = Arrays.asList("first", "second", "third", "fourth", "fifth").stream();
//        Stream<String> stream5 = new ArrayList(Arrays.asList("first", "second", "third", "fourth", "fifth")).stream();
//​
//        List<String> collectedStrings = stream1
//                .filter(string -> string.length() > 5)
//                .map(string -> string.toUpperCase())
//                .collect(Collectors.toList());
//​
//        Stream<String> stringStream = stream1.filter(string -> string.length() > 5);
//        Stream<String> stringStreamPhase2 = stringStream.map(string -> string.toUpperCase());
//        List<String> collect = stringStreamPhase2.collect(Collectors.toList());
//​
//        //tutaj bedzie error stream has already been operated upon or closed
//        //wynika to z faktu ze wywolujac operacje terminalna - konczca typu collect()
//        //zamykamy stream i nie mozemy go ponownie uzyc.
//
//        Set<String> collectToSet = stringStreamPhase2.collect(Collectors.toSet());
//​
//        System.out.println(collectedStrings);
//    }
//​
//        ​
//}
//
