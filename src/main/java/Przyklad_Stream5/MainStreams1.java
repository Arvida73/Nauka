package Przyklad_Stream5;

import java.util.Arrays;
import java.util.List;

public class MainStreams1 {
    public static void main(String[] args) {
        List<Streams1> streams1List = Arrays.asList(
                new Streams1("first",1),
                new Streams1("second",1),
                new Streams1("third",1),
                new Streams1("fourth",1),
                new Streams1("fifth",1)
        );
        streams1List.stream()
                .filter(streams1 -> streams1.name.length() > 5 )
                .map(streams1 -> streams1.name.toUpperCase())
                // .collect(Collectors.toList()) to jest inne zamknecie streama
                .forEach(System.out::println);



}
}

