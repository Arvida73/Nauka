package Przyklad_Stream5;

public class Streams1 {
   public final String name;
   public final int parzyste;

    public Streams1(String name, int parzyste) {
        this.name = name;
        this.parzyste = parzyste;
    }

    public String getName() {
        return name;
    }

}
