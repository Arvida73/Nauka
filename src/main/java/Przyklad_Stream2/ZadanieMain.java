package Przyklad_Stream2;

import java.util.*;
import java.util.stream.Collectors;

public class ZadanieMain {
    public static void main(String[] args) {
        List<Zadanie> integerList1 = Arrays.asList(
                new Zadanie("Adam", "Kowal", 15, 185),
                new Zadanie("Piotrek", "Kowalczyk", 21, 175),
                new Zadanie("Michał", "Kowalski", 25, 205)
        );

        // zadanie 1
        // Z listy osób wypisać imię i nazwisko osób, których wiek jest większy od 20 lub wzrost jest większy niż 1.75.

        integerList1.stream()
                .filter(zadanie -> zadanie.getAge() > 20 || zadanie.getHeight() > 175)
                .map(zadanie -> zadanie.getName())
                .forEach(System.out::println);

        //zadanie 2 wyfiltrować osoby tak, żeby zostały osoby o wieku mniejszym niż 18.
        // Następnie przypisać do zmiennej typu List<String> imię oraz nazwiska tych osób.

        integerList1.stream()
                .filter(zadanie -> zadanie.getAge() < 18)
                .map(zadanie -> zadanie.getName()+" "+zadanie.getSurname())
                .forEach(System.out::println);
        //Zadanie 3 Sprawdź, czy w liście osób istnieje osoba o nazwisku "Kowalski"
        integerList1.stream()
                .filter(zadanie -> zadanie.getSurname().equals("Kowalski"))
                .map(zadanie -> zadanie.getSurname())
                .forEach(System.out::println);
        //Zadanie 4 Sprawdź, czy w liście osób wszystkie osoby mają mniej niż 2 metry wzrostu.
        integerList1.stream()
                .filter(zadanie -> zadanie.getHeight()==200 )
                .map(zadanie -> zadanie.getName()+" "+zadanie.getSurname())
                .forEach(System.out::println);

        //Zadanie 5 posortuj listę osób w następujący sposób -
        // pierw nazwisko alfabetycznie rosnąco, następnie imię.

        integerList1.stream()
                .sorted(Comparator.comparing(Zadanie::getSurname)
                        .thenComparing(Zadanie::getName))
                .collect(Collectors.toList());

//                .filter(zadanie -> zadanie.comapreTo())
//                .map(zadanie -> zadanie.getSurname())
//                .forEach(System.out::println);

      //Zadanie 6 Skonwertować listę do mapy, której kluczem będzie imię i nazwisko,
        // a wartością będzie wiek osoby. (wygooglować sposób konwersji strumienia na mapę).
       Map<String, Integer> collect = integerList1.stream()
                .collect(Collectors.toMap(
                        zadanie -> zadanie.getSurname()+ " " + zadanie.getName(),
                        zadania -> zadania.getAge()
                ));
        System.out.println(collect);

        //Zadanie 7 Posortuj listę list liczb całkowitych za pomocą sumy ich elementów.

        List<Integer> list1 = Arrays.asList(1,2,3,4,5);
        List<Integer> list2 = Arrays.asList(6,8,9,10,11,12);
        List<List<Integer>> lists = new ArrayList<>();
        lists.add(list1);
        lists.add(list2);
        lists.stream()
                .sorted((x,y)-> {
                    int sumOfFirstElement = x.stream().reduce(0,Integer::sum);
                    int sumOfSecondElement = y.stream().reduce(0,Integer::sum);
                    return Integer.compare(sumOfFirstElement,sumOfSecondElement);

                }).forEach(System.out::println);
    }
}
