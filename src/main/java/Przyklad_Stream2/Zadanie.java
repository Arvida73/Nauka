package Przyklad_Stream2;

public class Zadanie {
    private String name;
    private String surname;
    private int age;
    private float height;


    public Zadanie(String name, String surname, int age, float height) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }
public int comapreTo(Zadanie o){
        return this.name.compareTo(o.surname+" "+o.getName());
}

}
