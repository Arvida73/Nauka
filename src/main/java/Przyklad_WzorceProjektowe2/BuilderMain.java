package Przyklad_WzorceProjektowe2;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author Marcin Szewczyk, 14.01.2020
 */
public class BuilderMain {

    public static void main(String[] args) {
        TransferBulider bulider = new TransferBulider();
        bulider.setPayerAccount("122342");
        bulider.setReceiverAccount("122342");
        bulider.setAmount(BigDecimal.valueOf(312));
        bulider.setCurrency("pln");
        bulider.setTradeDate(LocalDate.of(2020,1,12));
        bulider.setReceiverName("Stefan");


        Transfer transfer = bulider.bulid();

        System.out.println("Wysłano przelew: " + transfer);
    }

}
