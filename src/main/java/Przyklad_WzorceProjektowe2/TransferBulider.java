package Przyklad_WzorceProjektowe2;

import java.math.BigDecimal;
import java.time.LocalDate;

public class TransferBulider {
    String payerAccount;
    String receiverAccount;
    BigDecimal amount;
    String currency;
    LocalDate tradeDate;
    String title;
    String transactionType;
    String receiverName;
    String receiverAddress;

    public Transfer bulid(){
        return new Transfer(payerAccount,
                receiverAccount,
                amount,
                currency,
                tradeDate,
                title,
                transactionType,
                receiverName,
                receiverAddress);
    }

    public void setPayerAccount(String payerAccount) {
        if (payerAccount == null || receiverAccount == null || amount == null || currency == null || tradeDate == null || title == null)  {
            return;
        }else {
            this.payerAccount= payerAccount;
        }
        this.payerAccount = payerAccount;
    }

    public void setReceiverAccount(String receiverAccount) {
        this.receiverAccount = receiverAccount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setTradeDate(LocalDate tradeDate) {
        this.tradeDate = tradeDate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }
}
