package Przyklad_Stream;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MainBook {
    public static void main(String[] args) {

        List<Book> bookList = Arrays.asList(
            new Book("Olga Tokarczuk","Ksiegi Jakubowe",320,38,34,"ADVENTURE"),
                new Book("Alex Borht","Szeptacz",400,50,31,"THRILLER"),
                new Book("Soren Sveistrup","Kasztanowy ludzik",256,80,53,"SCIENCE"),
                new Book("Blanka Lipińska","365 dni",540,120,52,"FANTASY")
        );
        Wiek_50(bookList);
        Cena_30(bookList);
        Cena_50_Strony_300(bookList);
        type_book_wiek_30(bookList);
        check_2_name_tittle(bookList);
        type_string();
        show_allprice(bookList);
        Sort_by_tittle(bookList);
        //Zadanie 9
        // wyswiwetl sume wszystkich stron z gatunku fantasy
//        List<Book> wynik1 = bookList.stream()
//                .filter(book -> book.type_book().equals("Fantasy"))
//                        .map(book -> book.pages);
//        System.out.println(wynik1);


    }

    private static void Sort_by_tittle(List<Book> bookList) {
        //Zadanie 8 posortuj liste ksiazek wedlug tytulow
        //posortuj liste ksiazek wedlug nazwiska autora
        List<Book> sortowane = bookList.stream()
                .sorted(Comparator.comparing(Book::getTitle)
                      .thenComparing(Book::getAuthor))
                .collect(Collectors.toList()
                );
        System.out.println(sortowane);
    }

    private static void show_allprice(List<Book> bookList) {
        //Zadanie 7 wyswietl cene wszystkich ksiazek napisanych przez autora,
        // ktorego nazwisko rozpoczyna sie na 'B' oraz ma wiek > 50 lat

        long wynik = bookList.stream()
                 .filter(book -> book.author.startsWith("B") && book.Age_author>50)
                 .map(book -> book.price)
                 .reduce(0L,(x,y)->x+y);
        System.out.println(wynik);
    }


    private static void type_string() {
        //Zadanie 6 Na podstawie listy ksiazek utworz liste typu String,
        // gdzie kazdy string przedstawi nastepujaco informacje: imie i nazwisko autora, tytul i cena


    }

    private static void check_2_name_tittle(List<Book> bookList) {
        //Zadanie 5 wyswietl ksiazki ktorych nazwa sklada sie wiecej niz jednego slowa
        bookList.stream()
                .filter(book -> book.title.contains(" "))
                .map(book -> book.title)
                        .forEach(System.out::println);
    }

    private static void type_book_wiek_30(List<Book> bookList) {
        //Zadanie 4 wyswietl ksiazki ktore naleza do jednego z dwoch gatunkow oraz autor ma ponad 30 lat
        bookList.stream()
                .filter(book -> book.type_book.equals("ADVENTURE") && book.Age_author >= 30)
                .map(book -> book.title)
                .forEach(System.out::println);
    }

    private static void Cena_50_Strony_300(List<Book> bookList) {
        //Zadanie 3 wyswietl ksiazki, ktorych cena jest mniejsza niz 50 oraz liczba stron wieksza niz 300
        bookList.stream()
                .filter(book -> book.price < 50 && book.pages > 300)
                .map(book -> book.title)
                .forEach(System.out::println);
    }

    private static void Cena_30(List<Book> bookList) {
        //Zadanie 2 wyswietl ksiazki ktorych cena jest wieksza niz 30
        bookList.stream()
                .filter(book -> book.price > 30)
                .map(book -> book.title)
                .forEach(System.out::println);
    }

    private static void Wiek_50(List<Book> bookList) {
        // Zadanie 1 wyswietl ksiazki ktorych autor ma ponad 50 lat
        bookList.stream()
                .filter(book -> book.Age_author > 50)
                .map(book -> book.author)
                .forEach(System.out::println);
    }
}
