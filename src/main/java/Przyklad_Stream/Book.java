package Przyklad_Stream;

public class Book {
    public String author;
    public String title;
    public long pages;
    public long price;
    public int Age_author;
    public String type_book;

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public long getPages() {
        return pages;
    }

    public long getPrice() {
        return price;
    }

    public int getAge_author() {
        return Age_author;
    }

    public String getType_book() {
        return type_book;
    }

    public Book(String author, String title, long pages, long price, int age_author, String type_book) {
        this.author = author;
        this.title = title;
        this.pages = pages;
        this.price = price;
        Age_author = age_author;
        this.type_book = type_book;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", pages=" + pages +
                ", price=" + price +
                ", Age_author=" + Age_author +
                ", type_book='" + type_book + '\'' +
                '}';
    }
}
