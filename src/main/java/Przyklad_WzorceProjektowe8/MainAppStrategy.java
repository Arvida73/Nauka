package Przyklad_WzorceProjektowe8;

public class MainAppStrategy {

    public static void main(String[] args) {

        Chef hardChef = new Chef("Gordon Gessler",new BoillingHardEggStrategy());
        Chef softChef = new Chef("Podest Amaro",new BoillSoftEggStrategy());

        hardChef.cook();
        softChef.cook();

    }


}
