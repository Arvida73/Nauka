package Przyklad_WzorceProjektowe8;

public class Chef {

    private BolingEggStrategy bolingEggStrategy;

    private String name;

    public Chef(String name,BolingEggStrategy bolingEggStrategy) {
        this.bolingEggStrategy = bolingEggStrategy;
        this.name = name;
    }

    public void cook(){
        System.out.println(name + " nastawia wodę");
        bolingEggStrategy.boilEgg();
        System.out.println(name + " serwuje jajko gościom");
    }
}
