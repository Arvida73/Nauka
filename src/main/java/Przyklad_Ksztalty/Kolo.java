package Przyklad_Ksztalty;

public class Kolo implements Shape {
    private int promien;

    public Kolo(int promien) {
        this.promien = promien;
    }

    @Override
    public double calculateArea() {
        return 3.14 * promien * promien;
    }
}
