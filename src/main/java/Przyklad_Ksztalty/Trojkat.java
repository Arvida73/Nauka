package Przyklad_Ksztalty;

public class Trojkat implements Shape {
    private double a;
    private double b;

    public Trojkat(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculateArea() {
        return a*b/2;
    }
}
