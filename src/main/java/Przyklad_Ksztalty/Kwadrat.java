package Przyklad_Ksztalty;

public class Kwadrat implements Shape {
    private int a;

    public Kwadrat(int a) {
        this.a = a;
    }

    @Override
    public double calculateArea() {
        return a * a;
    }

    @Override
    public String toString() {
        return "kwadrat, bok = " + a; // przyjmowanie argumentow do konstruktora .. argumenty
    }
}
