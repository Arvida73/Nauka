package Przyklad_Ksztalty;

public class Kula implements Shape3D {
    private double R;

    public Kula(double r) {
        R = r;
    }

    @Override
    public double calculateVolume() {
        return 4 * 3.14 * R * R;
    }
}
