package Przyklad_Ksztalty;

public class MainShape {
    public static void main(String[] args) {


        Kolo biale = new Kolo(2);
        biale.calculateArea();
        System.out.println("Pole koła to : " +biale.calculateArea());

        Szescian czerwony = new Szescian(2);
        czerwony.calculateVolume();
        System.out.println("Pole szescianu to : " + czerwony.calculateVolume());

        Kula czarna = new Kula(2);
        czarna.calculateVolume();
        System.out.println("Pole kuli to : " + czarna.calculateVolume());

        Kwadrat zielony = new Kwadrat(2);
        zielony.calculateArea();
        System.out.println("Pole kwadratu to : " +zielony.calculateArea());
        System.out.println(zielony.toString());
        Trojkat zolty = new Trojkat(2,2);
        zolty.calculateArea();
        System.out.println("Pole trojkata to : " +zolty.calculateArea());

    }
}
