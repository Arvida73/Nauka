package Przyklad_Ksztalty;

public class Szescian implements Shape3D  {
    private double a;

    public Szescian(double a) {
        this.a = a;
    }

    @Override
    public double calculateVolume() {
        return (6*a)*2 ;
    }
}
