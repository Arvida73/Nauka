package Przyklad_DaneOsobowe;

public class Main {
    public static void main(String[] args) {
        Job job = new Job("Programista",10_000);
        User user = new User("Kamil");
        user.setJob(job);

        System.out.println(user.getName() + " pracuje jako : " + user.getJob().getTitle() + " za: " + user.getJob().getSalary());

        user.setAdres("Krakowska",1,2,35330,"krakow");
    String adressInfo = user.getAdressInfo();
        System.out.println(adressInfo);
    }
   // private static void printUserInfo(User user) {...}
}
