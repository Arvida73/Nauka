package Przyklad_DaneOsobowe;

public class User {
    private String name;
    private int age;
    private String sex;
    private int paidAmmout;
    private Job job; // kompozycja
    private Adres adres = new Adres();



    public User(String name) {
        this.name = name;
    }

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getPaidAmmout() {
        return paidAmmout;
    }

    public void setPaidAmmout(int paidAmmout) {
        this.paidAmmout = paidAmmout;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public void setAdres(String ulica, int numerDomu, int numerMieszkania, int kodPocztowy, String Miasto)  {
         this.adres.ulica = ulica;
         this.adres.numerDomu = numerDomu;
         this.adres.numerMieszkania = numerMieszkania;
         this.adres.kodPocztowy = kodPocztowy;
         this.adres.Miasto = Miasto;


}
public String getAdressInfo() {
        return this.adres.toString();
}


    public class Adres {
        private String ulica;
       private int numerDomu;
       private int numerMieszkania;
       private int kodPocztowy;
      private String Miasto;

        @Override
        public String toString() {
            return "Adres{" +
                    "ulica='" + ulica + '\'' +
                    ", numerDomu=" + numerDomu +
                    ", numerMieszkania=" + numerMieszkania +
                    ", kodPocztowy=" + kodPocztowy +
                    ", Miasto=" + Miasto +
                    '}';
        }
    }

}

