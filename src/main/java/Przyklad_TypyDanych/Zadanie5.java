package Przyklad_TypyDanych;

import java.util.Scanner;

public class Zadanie5 {
    public static void main(String[] args) {
        int rok = 2300;

        if ((rok % 4 == 0 && rok % 100 != 0) || rok % 400 == 0) {
            System.out.println("Rok " + rok + " jest przestepny");
        } else {
            System.out.println("Rok " + rok + " nie jest przestepny");
        }
    }
}
