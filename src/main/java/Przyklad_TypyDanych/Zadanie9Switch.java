package Przyklad_TypyDanych;

import java.util.Scanner;

public class Zadanie9Switch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj liczbe: ");
        int liczba = scanner.nextInt();

        switch (liczba) {
            case 0:
                System.out.println(")");
                break;
            case 1:
                System.out.println("!");
                break;
            case 2:
                System.out.println("@");
                break;
            case 3:
                System.out.println("#");
                break;
            case 4:
                System.out.println("$");
                break;
            case 5:
                System.out.println("%");
                break;
            case 6:
                System.out.println("^");
                break;
            case 7:
                System.out.println("&");
                break;
            case 8:
                System.out.println("*");
                break;
            case 9:
                System.out.println(")");
                break;
        }
    }
}
