package Przyklad_TypyDanych;

import java.util.Scanner;

public class Zadanie8 {
    public static void main(String[] args) {
        double liczba1 = -12.5;
        double liczba2 = 0.0001;
        double liczba3 = 1234567;

        int liczbyWiekszaOdZera = 0;
        if (liczba1 > 0) {
            ++liczbyWiekszaOdZera;
        }
        if (liczba2 > 0) {
            liczbyWiekszaOdZera += 1;
        }
        if (liczba3 > 0) {
            liczbyWiekszaOdZera = liczbyWiekszaOdZera + 1;
        }

        System.out.println("Ilosc liczb wiekszych od zera: " + liczbyWiekszaOdZera);
    }
}
