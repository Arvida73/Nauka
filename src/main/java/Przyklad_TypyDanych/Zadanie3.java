package Przyklad_TypyDanych;

public class Zadanie3 {
    public static void main(String[] args) {
        int liczba = 123;

        if (liczba % 2 == 0) {
            System.out.println("Liczba: " + liczba + " jest parzysta");
        } else {
            System.out.println("Liczba: " + liczba + " jest nieparzysta");
        }
    }
}
