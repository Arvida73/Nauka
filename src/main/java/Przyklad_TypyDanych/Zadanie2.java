package Przyklad_TypyDanych;

public class Zadanie2 {
    public static void main(String[] args) {
        int liczba1 = 123;
        int liczba2 = 234;

        if (liczba1 > liczba2) {
            System.out.println("Wieksza liczba: " + liczba1);
        } else if (liczba1 == liczba2){
            System.out.println("Wieksza liczba: " + liczba2);
        } else {
            System.out.println("Liczby sa rowne.");
        }
    }
}
