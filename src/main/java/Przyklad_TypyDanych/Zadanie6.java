package Przyklad_TypyDanych;

import java.util.Scanner;

public class Zadanie6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj wspolczynnik A: ");
        double wspolczynnikA = scanner.nextDouble();
        System.out.print("Podaj wspolczynnik B: ");
        double wspolczynnikB = scanner.nextDouble();
        System.out.print("Podaj wspolczynnik C: ");
        double wspolczynnikC = scanner.nextDouble();

        // Obliczamy delte
        double detla = (wspolczynnikB * wspolczynnikB - 4 * wspolczynnikA * wspolczynnikC);

        if (detla < 0) {
            System.out.println("Rownanie nie ma rozwiazan");
        }
        if (detla == 0) {
            double rozwiazanie = (-1 * wspolczynnikB) / (2 * wspolczynnikA);
            System.out.println("Rownanie ma jedno rozwiazanie: " + rozwiazanie);
        }

        if (detla > 0) {
            double pierwiastekZDelta = Math.sqrt(detla);
            double rozwiazanie1 = (-1 * wspolczynnikB + pierwiastekZDelta) / (2 * wspolczynnikA);
            double rozwiazanie2 = (-1 * wspolczynnikB - pierwiastekZDelta) / (2 * wspolczynnikA);
            System.out.println("Rownanie ma dwa rozwiazania: 1. " + rozwiazanie1 + "; 2. " + rozwiazanie2);
        }
    }
}
