package Przyklad_TypyDanych;

import java.util.Scanner;

public class Zadanie9If {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj liczbe: ");
        int liczba = scanner.nextInt();

        if (liczba == 0) {
            System.out.println(")");
        } else if (liczba == 1) {
            System.out.println("!");
        } else if (liczba == 2) {
            System.out.println("@");
        } else if (liczba == 3) {
            System.out.println("#");
        } else if (liczba == 4) {
            System.out.println("$");
        } else if (liczba == 5) {
            System.out.println("%");
        } else if (liczba == 6) {
            System.out.println("^");
        } else if (liczba == 7) {
            System.out.println("&");
        } else if (liczba == 8) {
            System.out.println("*");
        }  else if (liczba == 9) {
            System.out.println(")");
        }
    }
}
