package Przyklad_TypyDanych;

import java.util.Scanner;

public class Zadanie10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj pierwsza liczbe: ");
        int zmienna1 = scanner.nextInt();

        System.out.print("Podaj druga liczbe: ");
        int zmienna2 = scanner.nextInt();

        System.out.println("Przed zmiana. Pierwsza liczba: " + zmienna1 + " druga liczba: " + zmienna2);

        int tymczasowaZmienna = zmienna1;
        zmienna1 = zmienna2;
        zmienna2 = tymczasowaZmienna;

        System.out.println("Po zmianie. Pierwsza liczba: " + zmienna1 + " druga liczba: " + zmienna2);
    }
}
