package Przyklad_TypyDanych;

import java.util.Scanner;

public class Zadanie4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj pierwsza liczbe: ");
        int pierwszaLiczba = scanner.nextInt();
        System.out.print("Podaj druga liczbe: ");
        int drugaLiczba = scanner.nextInt();

        int suma = pierwszaLiczba + drugaLiczba;
        System.out.println(pierwszaLiczba + " + " + drugaLiczba + " = " + suma);
    }
}
