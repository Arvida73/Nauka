package Przyklad_TypyDanych;

public class Zadanie1 {
    public static void main(String[] args) {
        byte zmiennaByte = 2;
        short zmiennaShort = 1;
        int zmiennaInt = 123;
        long zmiennaLong = 12343567890L;
        char zmiennaChar = 'S';
        boolean zmiennaBoolean = false;
        float zmiennaFloat = 1.2345f;
        double zmiennaDouble = 1.2345678;

        System.out.println("Zmienna typu byte: " + zmiennaByte);
        System.out.println("Zmienna typu short: " + zmiennaShort);
        System.out.println("Zmienna typu int: " + zmiennaInt);
        System.out.println("Zmienna typu long: " + zmiennaLong);
        System.out.println("Zmienna typu char: " + zmiennaChar);
        System.out.println("Zmienna typu boolean: " + zmiennaBoolean);
        System.out.println("Zmienna typu float: " + zmiennaFloat);
        System.out.println("Zmienna typu double: " + zmiennaDouble);
    }
}
