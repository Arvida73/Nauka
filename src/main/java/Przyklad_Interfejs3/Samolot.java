package Przyklad_Interfejs3;

public class Samolot extends Pojazd0  implements Moveable, Flyable {
    public static Object Details;
    private final Details details = new Details();
    public Samolot() {
        super(800);
    }
    private int howEngine;
    public int getIloscPasazerow() {
        return 100;
    }

    public Samolot(int speed, int howEngine) {
        super(speed);
        this.howEngine = howEngine;
    }
    public  void mov() {
        System.out.println("Samolot - move");
    }
    public void setPrzebieg(int przebieg){
        details.setPrzebieg(przebieg);
      //  details.przebieg = przebieg;
    }
    public Details getDetails(){
        return details;
    }

    @Override
    public void fly() {

    }
    public static class Details {
        private  int przebieg;

        public int getPrzebieg() {
            return przebieg;
        }

        private void setPrzebieg(int przebieg) {
            this.przebieg = przebieg;
        }
    }
}
