package Przyklad_Interfejs3;

public abstract class Pojazd0 {
    private int maxSpeed;
    private  int speed;

    public int getMaxSpeed() {
        return maxSpeed;
    }
    public abstract  int getIloscPasazerow();
    public void printIloscPasazerow () {
       int  IloscPasazerow = getIloscPasazerow();
        System.out.println("Ilosc pasaerów = " + IloscPasazerow);
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        if (speed > maxSpeed) {
        }
        else  {this.speed = speed; }
        System.out.println("Tworze pojazd z aktualna predkoscia " + speed);
    }

    public Pojazd0(int speed) {
        this.maxSpeed= speed;
    }
}
