package Przyklad_Interfejs3;

public class Rower extends Pojazd0 {

    public Rower() {
        super(25);
    }

    private int shift;

    public int getIloscPasazerow() {
        return 1;
    }

    public Rower(int speed, int shift) {
        super(speed);
        this.shift = shift;
    }

    public void mov() {
        System.out.println("Samolot - move");
    }
}
