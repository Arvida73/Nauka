package Przyklad_StrukturyDanych3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Radix {

  public static void main(String[] args) {

    System.out.println("Tablica przed sortowaniem metoda sortowania Radix sort");

    int[] input = { 181, 51, 11, 33, 11, 39, 60, 2, 27, 24, 12 };

    System.out.println(Arrays.toString(input));

    radixSort(input);

    System.out.println("Tablica posortowana metoda Radix");

    System.out.println(Arrays.toString(input));

  }

  public static void radixSort(int[] input) {
    int RADIX = 10;

    //Delarowanie zmiennej kubek
    List<Integer>[] kubek = new ArrayList[RADIX];

    //petla for sprawdz dlugosc zmiennej listy kubek od i
    for (int i = 0; i < kubek.length; i++) {

      kubek[i] = new ArrayList<Integer>();
    }

    //sortowanienie
    boolean maxLength = false;
    int temp = -1, miejsce = 1;
    while (!maxLength) {
      maxLength = true;

      // potrzebuje zlczyc input
      for (Integer i : input) {
        temp = i / miejsce;
        kubek[temp % RADIX].add(i);
        if (maxLength && temp > 0) {
          maxLength = false;
        }
      }
      
      // potrzebuje puste listy do input array
      int a = 0;
      for (int b = 0; b < RADIX; b++) {
        for (Integer i : kubek[b]) {
          input[a++] = i;
        }
        kubek[b].clear();
      }

      // przesuniecie do nastepnego miejsca
      miejsce *= RADIX;
    }
  }
}


