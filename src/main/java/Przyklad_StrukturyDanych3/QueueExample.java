package Przyklad_StrukturyDanych3;

import java.util.LinkedList;
import java.util.Queue;

public class QueueExample {
    public static void main(String[] args) {

        Queue<String> waitingQueue = new LinkedList<>();


        waitingQueue.add("Damian");
        waitingQueue.add("Kamil");
        waitingQueue.add("Krzysztof");
        waitingQueue.add("Tomasz");
        waitingQueue.add("Michal");

        System.out.println("WaitingQueue : " + waitingQueue);


        String name = waitingQueue.remove();
        System.out.println("Wyszedl z kolejki : " + name + " | Nowa osoba w kolejce : " + waitingQueue);

        name = waitingQueue.poll();
        System.out.println("Wyszedl z kolejki : " + name + " | Nowa osoba w kolejce : " + waitingQueue);
    }
}