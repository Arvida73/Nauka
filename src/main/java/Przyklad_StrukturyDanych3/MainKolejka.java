package Przyklad_StrukturyDanych3;

public class MainKolejka {
    public static void main(String[] args) {
        //dodajemy osoby
        Customlist.Person p1 = new Customlist.Person("A","B");
        Customlist.Person p2 = new Customlist.Person("C","D");
        Customlist.Person p3 = new Customlist.Person("E","F");
        Customlist.Person p4 = new Customlist.Person("G","H");
        Customlist list = new Customlist();
        //dodajemy do listy osoby
        list.add(p1);
        list.add(p2);
        list.add(p3);
        list.add(p4);
        //wyswietlamy osoby i usuwamy
        System.out.println(list.remove(4));
//        System.out.println(list.remove(4));
//        System.out.println(list.remove(4));
//        System.out.println(list.remove(4));


    }
}


class Customlist {
    private Person last;
    private int size;

    public void add(Person person) {
        if (last == null) {
            last = person;
        } else {
            person.setPrev(last);
            last = person;
        }
        size++;
    }

    public Person remove(int osobaRemove) {
        Person result = null;
        if (size == 1) {
            result = last;
            last = null;
        } else {
            Person temp = last;

//            while (temp.getPrev().getPrev() != null) {
//                temp = temp.getPrev();
                //System.out.println(licznik);
                for (int i = 0; i < osobaRemove - 2; i++) {
                    temp = temp.getPrev();

                }

                result = temp.getPrev();
                temp.setPrev(temp.getPrev().getPrev());
//            }
        }
        size--;
        return result;
    }

    public int size() {
        return size;
    }

    static class Person {
        private String name;
        private String surname;
        private Person prev;


        public Person(String name, String surname) {
            this.name = name;
            this.surname = surname;
        }

        public Person getPrev() {
            return prev;
        }

        public void setPrev(Person prev) {
            this.prev = prev;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", surname='" + surname + '\'' +
                    '}';
        }
    }
}
