package Przyklad_StrukturyDanych3;

class Quicksort {

    static void qsort(int items[]) {
        qs(items, 0, items.length-1);
    }

    // Rekurencyjna wersja Quicksort sortująca tablicę intow.
    private static void qs(int items[], int left, int right) {
        int i, j;
        int x, y;
        i = left;
        j = right;
        x = items[(left + right) / 2];
        do {
            while ((items[i] < x) && (i < right)) i++;
            while ((x < items[j]) && (j > left)) j--;
            if (i <= j) {
                y = items[i];
                items[i] = items[j];
                items[j] = y;
                i++;
                j--;
            }
        } while (i <= j);
        if (left < j) qs(items, left, j);
        if (i < right) qs(items, i, right);
    }
}
class QSDemo {
    public static void main(String args[]) {
        int a[] = {1,52,12,40,25,15,12,50,34,29};
        int i;
        System.out.print("Tablica przed posortowaniem: ");
        for(i=0; i < a.length; i++)
            System.out.print(a[i]);
        System.out.println();
        // sortuje tablicę
        Quicksort.qsort(a);
        System.out.print("Tablica posortowana: ");
        for(i=0; i < a.length; i++)
            System.out.print(a[i]);
    }
}