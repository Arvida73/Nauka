package Przyklad_Interfejs2;

import java.util.Scanner;

public class Main {

    private Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        Main main = new Main();
        Computation computation;

        if (main.shouldMultiply()) {
            computation = (Computation) new Multiplication(); // zaimplementuj brakującą klasę
        }
        else {
            computation = (Computation) new Addition(); // zaimplementuj brakującą klasę
        }

        double argument1 = main.getArgument();
        double argument2 = main.getArgument();

        double result = computation.compute(argument1, argument2);
        System.out.println("Wynik: " + result);
    }

    private boolean shouldMultiply() {
        System.out.println("Jaka operacje chcesz wykonac ? Napisz M jezeli mnozenie, w przypadku dodawania wcisnij dowolny klawisz ");
        return scanner.next().equals("M");

        // tutaj zapytaj użytkownika co chce zrobić (mnożenie czy dodawanie)
    }

    private double getArgument() {
        System.out.println("Podaj DWIE liczbe");
        return scanner.nextDouble();
        // tutaj pobierz liczbę od użytkownika
    }
}