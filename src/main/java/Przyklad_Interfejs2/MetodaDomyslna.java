package Przyklad_Interfejs2;

public interface MetodaDomyslna {
    void start();

    void setDuration(int durationInSeconds);

    boolean isFisnished();

    void setPower(int power);

    default String getName() {

        return "MicrovaweOwen";

    }
}
