package Przyklad_Interfejs2;

public interface Computation {
        double compute(double argument1, double argument2);
    }
