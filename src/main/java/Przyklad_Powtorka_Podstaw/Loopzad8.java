package Przyklad_Powtorka_Podstaw;

public class Loopzad8 {
    public static void main(String[] args) {
        int suma = 0;
        for (int liczba = -10; liczba < 100; liczba++) {
            if (liczba % 2 != 0) {
                suma = suma + liczba;
            }
        }
        System.out.println("suma " +suma);
    }
}
