package Przyklad_Powtorka_Podstaw;

import java.util.Scanner;

public class StringZad2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile stringow");
        int ileStringow = scanner.nextInt();
        scanner.nextLine();

        String polaczononytext = "";
        for (int i = 0; i < ileStringow; i++) {
            System.out.println("Podaj tresc");
            String text = scanner.nextLine();
            polaczononytext += text;
        }
        System.out.println("Polaczony tekst : " + polaczononytext);
    }
}
