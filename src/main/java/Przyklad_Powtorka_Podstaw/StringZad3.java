package Przyklad_Powtorka_Podstaw;

public class StringZad3 {
    public static void main(String[] args) {
        String text = "Piorytetowy";

        int liczbaW = 0;
        int liczbaR = 0;
        for(char litera : text.toCharArray()){
            if(litera == 'r'){
                liczbaR++;
            }
            if(litera == 'w'){
                liczbaW++;
            }
        }
        System.out.println("Liczba liter r : " + liczbaR);
        System.out.println("Liczba liter w : " + liczbaW);
    }
}
