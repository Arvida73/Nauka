package Przyklad_Kalkulator;

public class Kalkulator {
    private Kalkulator() {}
    public static int dodaj( int liczba1, int liczba2) {
        return liczba1 + liczba2;
    }
    public static double dodaj5(double liczba1,double liczba2) {// polimorfizm
        System.out.println("dodaj - double, dobule");
        return liczba1 + liczba2;
    }
    public static int mnozenie ( int liczba1, int liczba2) {
        return liczba1 * liczba2;
    }
    public static int dzielenie ( int liczba1, int liczba2){
            return liczba1 / liczba2;
        }
        public static int mnozenie1 ( int... wartosc) {
            int suma = 1;
            for (int wartosc1 : wartosc ) {// przechodznie przez tablice
                suma *= wartosc1;
            }
            return suma;
        }
        public static int dodawanie1 ( int... wartosc) {
            int suma = 0;
            for (int wartosc1 : wartosc ) {// przechodznie przez tablice
                suma += wartosc1;
            }
            return suma;
        }
        public static int dodaj2(int... liczby) {
        if (liczby.length <2 ) return -1;
        int suma = 0;
        for( int liczba : liczby) {
            suma += liczba;
        }
        return suma;
        }
        public static int dodaj3(int liczba1,int liczba2,int... liczby) {
        if (liczby.length <2 ) return -1;
        int suma = liczba1 + liczba2;
        for( int liczba : liczby) {
            suma += liczba;
        }
        return suma;
        }

    }