package Przyklad_Interfejs;

public class Kot implements Zwierze, RuchomeZwierze {

    public void dajGlos() {
        System.out.println("Kot - miau");

    }

    @Override
    public void ruszaj() {
        System.out.println("Ruszaj sie - kot");
    }
}
