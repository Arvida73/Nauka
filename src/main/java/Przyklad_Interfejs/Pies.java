package Przyklad_Interfejs;

public class Pies implements Zwierze, RuchomeZwierze {
    public void dajGlos() {
        System.out.println("Pies - hauhua");
    }

    @Override
    public void ruszaj() {
        System.out.println("Ruszaj sie - Pies");
    }
}
