package Przyklad_WzorceProjektowe12;

public class Soldier {

    String nameSoldier;
    AttackStrategy attackStrategy;
    DefendStrategy defendStrategy;
    public Soldier(String nameSoldier, AttackStrategy attackStrategy, DefendStrategy defendStrategy) {
        this.attackStrategy = attackStrategy;
        this.defendStrategy = defendStrategy;
        this.nameSoldier = nameSoldier;
    }

    public String getNameSoldier() {
        return nameSoldier;
    }
    void attack(){
        attackStrategy.attack();
    }
    void defend(){
        defendStrategy.defend();
    }
}
