package Przyklad_WzorceProjektowe12;

public class MainAttackStrategy {
    public static void main(String[] args) {

        Soldier soldier1 = new Soldier("Spartakus",new AttackAgressive(),new DefendAgressive());
        Soldier soldier2 = new Soldier("Napoleon",new AttackAgressive(),new DefendAgressive());

        soldier1.attack();
        System.out.println(soldier1.nameSoldier);
        soldier2.defend();
        System.out.println(soldier2.nameSoldier);


    }
}
